<?php

namespace App\Http\Controllers\DataMaster;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;

class LocationController extends Controller
{
    public function __construct() {
        $this->middleware(function ($request, $next) {
            if (!session('login')) {
                return redirect()->route('login')->withErrors(['error' => 'Please sign in to continue']);
                die();
            };
            return $next($request);
        });
    }

    public function index(Request $req) {
        try {
            $data = DB::table('building as a')
                ->join('location as b', 'b.location_id', '=', 'a.location_id')
                ->select('a.*', 'b.name AS location_name')
                ->where('a.deleted', 0)->get();

            $param = [
                'title' => 'Data Master > Location',
                'data' => $data
            ];
            return view('data-master.location.index', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function add(Request $req) {
        try {
            $param = [
                'title' => 'Data Master > Location > Add',
                'location' => DB::table('location')->where('deleted', 0)->get()
            ];

            if ($req->isMethod('post')) {
                $input = $req->all();
                unset($input['_token']);
                $location_id = $input['location_id'];
                $name = $input['name'];
                $messages = [
                    'location_id.unique' => 'Given district and name has already been taken',
                    'location_id.required' => 'District field is required',
                    'name.required' => 'Name field is required',
                ];
                $rules = [
                    'location_id' => [
                        'required',
                        Rule::unique('building')->where(function ($query) use($location_id,$name) {
                            return $query->where([
                                'location_id' => $location_id,
                                'name' => $name
                            ]);
                        })
                    ],
                    'name' => 'required|string|min:3|max:100'
                ];
                $validator = Validator::make($input, $rules, $messages);
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput();
                }
                DB::table('building')->insert($input);
                $success = ['success' => 'Data has been saved successfuly'];
                return redirect()->route('data-master.location.index')->withErrors($success);
            }


            return view('data-master.location.add', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function edit(Request $req) {
        try {
            $data = DB::table('building')->where('building_id', $req->id)->first();
            $location = DB::table('location')->where('deleted', 0)->get();
            $param = [
                'title' => 'Data Master > Location > Edit',
                'data' => $data,
                'location' => $location
            ];
            
            if ($req->isMethod('post')) {
                $input = $req->all();
                unset($input['_token']);
                $location_id = $input['location_id'];
                $name = $input['name'];
                $messages = [
                    'location_id.unique' => 'Given district and name has already been taken',
                    'location_id.required' => 'District field is required',
                    'name.required' => 'Name field is required',
                ];

                $rules = [
                    'location_id' => [
                        'required',
                        Rule::unique('building')->where(function ($query) use($location_id,$name) {
                            return $query->where([
                                'location_id' => $location_id,
                                'name' => $name
                            ]);
                        })->ignore($req->id, 'building_id')
                    ],
                    'name' => 'required|string|min:3|max:100'
                ];
                $validator = Validator::make($input, $rules, $messages);
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput();
                }
                DB::table('building')->where('building_id', $req->id)->update($input);
                $success = ['success' => 'Data has been updated successfuly'];
                return redirect()->route('data-master.location.index')->withErrors($success);
            }

            return view('data-master.location.edit', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function delete(Request $req) {
        try {
            DB::table('building')->where('building_id', $req->id)->update(['deleted' => 1]);
            $success = ['success' => 'Data has been deleted successfuly'];
            return redirect()->route('data-master.location.index')->withErrors($success);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function selectBuilding(Request $request) {
        $data = DB::table('building')
            ->where('name', 'like', '%'.$request->search.'%')
            ->where('location_id', $request->location_id)
            ->get();

        $output['results'] = array();
        for ($i=0; $i < count($data); $i++) { 
            $output['results'][] = [
                'id' => $data[$i]->building_id,
                'text' => $data[$i]->name
            ];
        }
        
        return \json_encode($output);
    }
}
