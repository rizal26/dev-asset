<?php

namespace App\Http\Controllers\DataMaster;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;

class EntityController extends Controller
{
    public function __construct() {
        $this->middleware(function ($request, $next) {
            if (!session('login')) {
                return redirect()->route('login')->withErrors(['error' => 'Please sign in to continue']);
                die();
            };
            return $next($request);
        });
    }

    public function index(Request $req) {
        try {
            $data = DB::table('entity')->where('deleted', 0)->get();
            $param = [
                'title' => 'Data Master > Entity',
                'data' => $data
            ];
            return view('data-master.entity.index', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function add(Request $req) {
        try {
            $param = [
                'title' => 'Data Master > Entity > Add'
            ];

            if ($req->isMethod('post')) {
                $input = $req->all();
                unset($input['_token']);
                $messages = [
                    'code.unique' => 'Code has already been taken',
                    'code.required' => 'Code field is required',
                    'name.required' => 'Name field is required',
                ];
                $rules = [
                    'code' => 'required|string|max:5|unique:entity,code',
                    'name' => 'required|string|min:3|max:100'
                ];
                $validator = Validator::make($input, $rules, $messages);
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput();
                }
                DB::table('entity')->insert($input);
                $success = ['success' => 'Data has been saved successfuly'];
                return redirect()->route('data-master.entity.index')->withErrors($success);
            }


            return view('data-master.entity.add', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function edit(Request $req) {
        try {
            $data = DB::table('entity')->where('entity_id', $req->id)->first();
            $param = [
                'title' => 'Data Master > Entity > Edit',
                'data' => $data
            ];

            if ($req->isMethod('post')) {
                $input = $req->all();
                unset($input['_token']);
                $messages = [
                    'code.unique' => 'Code has already been taken',
                    'code.required' => 'Code field is required',
                    'name.required' => 'Name field is required',
                ];
                $rules = [
                    'code' => 'required|string|max:5|unique:entity,code,' . $req->id . ',entity_id',
                    'name' => 'required|string|min:3|max:100'
                ];
                $validator = Validator::make($input, $rules, $messages);
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput();
                }
                DB::table('entity')->where('entity_id', $req->id)->update($input);
                $success = ['success' => 'Data has been updated successfuly'];
                return redirect()->route('data-master.entity.index')->withErrors($success);
            }

            return view('data-master.entity.edit', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function delete(Request $req) {
        try {
            DB::table('entity')->where('entity_id', $req->id)->update(['deleted' => 1]);
            $success = ['success' => 'Data has been deleted successfuly'];
            return redirect()->route('data-master.entity.index')->withErrors($success);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }
}
