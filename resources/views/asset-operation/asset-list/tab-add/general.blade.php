                                        <div role="tabpanel" class="tab-pane {{ $generalLink=='active'?'active':'fade' }}" id="account-vertical-general" aria-labelledby="account-pill-general" aria-expanded="{{ $generalAriaExpand }}">
                                            <!-- form -->
                                            <form class="validate-form" action="{{ route('asset-operation.asset-list.add') }}?tab=general" method="POST" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Asset Name</label>
                                                            <select name="m_asset_id" class="form-control select2" {{ $disabled }}>
                                                                <option></option>
                                                                @foreach ($m_asset as $item)
                                                                    <option value="{{ $item->m_asset_id }}" 
                                                                        {{ $item->m_asset_id==old('m_asset_id')?'selected':'' }}>{{ $item->nama }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Category</label>
                                                            <input type="text" id="categoryName" class="form-control" value="{{ old('category') }}" readonly/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Brand</label>
                                                            <input type="text" class="form-control" name="brand" value="{{ old('brand') }}" placeholder="Brand" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Type / Spesification</label>
                                                            <input type="text" class="form-control" name="spec" value="{{ old('spec') }}" placeholder="Type / Spesification" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Serial Number</label>
                                                            <input type="text" class="form-control" name="sn" value="{{ old('sn') }}" placeholder="Serial Number" />
                                                        </div>
                                                    </div>
                                                    {{-- @if ($data->category_id == 1 || $data->category_code == 'TE') --}}
                                                    <div class="col-12 col-sm-6 transport-form">
                                                        <div class="form-group">
                                                            <label for="account-name">NOPOL</label>
                                                            <input type="text" class="form-control" name="nopol" value="{{ old('nopol') }}" placeholder="Plat Nomor Polisi" />
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6 transport-form">
                                                        <div class="form-group">
                                                            <label for="account-name">Color</label>
                                                            <input type="text" class="form-control" name="color" value="{{ old('color') }}" placeholder="Color" />
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6 transport-form">
                                                        <div class="form-group">
                                                            <label for="account-name">CC</label>
                                                            <input type="text" class="form-control" name="cc" value="{{ old('cc') }}" placeholder="CC" />
                                                        </div>
                                                    </div>
                                                    {{-- @endif --}}
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Status</label>
                                                            <select name="usage_status" class="form_control select2" >
                                                                <option></option>
                                                                @php
                                                                    $optStatus = [
                                                                        1 => 'In Used',
                                                                        2 => 'Vacant',
                                                                        3 => 'Under Maintenance',
                                                                        4 => 'Leased'
                                                                    ];
                                                                @endphp
                                                                @foreach ($optStatus as $key => $item)
                                                                    <option value="{{ $key }}" {{ $key==old('status')?'selected':'' }}>{{ $item }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Condition</label>
                                                            <select name="condition" class="form_control select2" >
                                                                <option></option>
                                                                @php
                                                                    $optCondition = [
                                                                        1 => 'Good',
                                                                        2 => 'Slightly Damage',
                                                                        3 => 'Totally Damage'
                                                                    ];
                                                                @endphp
                                                                @foreach ($optCondition as $key => $item)
                                                                    <option value="{{ $key }}" {{ $key==old('condition')?'selected':'' }}>{{ $item }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Upload New Image</label>
                                                            <input type="file" name="image" class="form-control" accept=".jpg, .jpeg, .png" >
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <button type="button" id="nextFromGeneral" class="btn btn-primary mt-1 mr-1">Next</button>
                                                        <a href="{{ route('asset-operation.asset-list.index') }}" class="btn btn-outline-secondary mt-2">Cancel</a>
                                                    </div>
                                                </div>
                                            </form>
                                            <!--/ form -->
                                        </div>
                                        <script>
                                            $(document).ready(function () {
                                                $('#nextFromGeneral').click(function (e) { 
                                                    e.preventDefault();
                                                    $('#account-vertical-location').addClass('active');                                                    
                                                    $('#account-vertical-location').addClass('fade');                                                    
                                                    $('#account-vertical-location').addClass('show');                                                    
                                                    $('#account-vertical-location').attr('aria-expanded', 'true');
                                                    $('#account-pill-location').addClass('active');                                                    
                                                    $('#account-pill-location').attr('aria-expanded', 'true');

                                                    $('#account-vertical-general').removeClass('active');                                                    
                                                    $('#account-vertical-general').removeClass('show');                                                    
                                                    $('#account-vertical-general').removeClass('fade');                                                    
                                                    $('#account-vertical-general').attr('aria-expanded', 'false');                                                    
                                                    $('#account-pill-general').removeClass('active');                                                    
                                                    $('#account-pill-general').attr('aria-expanded', 'false');                                                    


                                                });
                                            });
                                        </script>
