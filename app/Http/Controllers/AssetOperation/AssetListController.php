<?php

namespace App\Http\Controllers\AssetOperation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Facades\Validator;
use App\Exports\AssetListExport;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class AssetListController extends Controller
{
    public function __construct() {
        $this->middleware(function ($request, $next) {
            if (!session('login')) {
                return redirect()->route('login')->withErrors(['error' => 'Please sign in to continue']);
                die();
            };
            return $next($request);
        });
    }

    public function index(Request $req) {
        try {
            $where = ['deleted' => 0];
            if (in_array(session('users')->role, [2, 3])) {
                $where['location_id'] = session('users')->location_id;
                $where['department_id'] = session('users')->department_id;
            }
            $data = DB::table('v_asset_list')
                ->when($req->has("search"), function($q)use($req){
                    return $q->where("asset_name","like","%".$req->get("search")."%")
                        ->orWhere("asset_code","like","%".$req->get("search")."%")
                        ->orWhere("brand","like","%".$req->get("search")."%")
                        ->orWhere("spec","like","%".$req->get("search")."%")
                        ->orWhere("dept_name","like","%".$req->get("search")."%")
                        ->orWhere("location_name","like","%".$req->get("search")."%")
                        ->orWhere("assigned_user","like","%".$req->get("search")."%");
                })
                ->where($where)
                ->orderBy('asset_id', 'DESC')
                ->paginate($req->has("rows")?$req->get("rows"):10);
            $data->appends(['search' => $req->get("search")]);
            $data->appends(['rows' => $req->get("rows")]);

            $param = [
                'title' => 'Asset Operation > Asset List',
                'data' => $data
            ];
            return view('asset-operation.asset-list.index', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function add(Request $req) {
        try {
            $param = [
                'title' => 'Asset Operation > Asset List > Add',
                'm_asset' => DB::table('m_asset')->where('deleted', 0)->get(),
                'location' => DB::table('location')->where('deleted', 0)->get(),
                'entity' => DB::table('entity')->where('deleted', 0)->get(),
                'supplier' => DB::table('supplier')->where('deleted', 0)->get(),
                'users' => DB::table('users')->where('deleted', 0)->get()
            ];
            return view('asset-operation.asset-list.add', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function image(Request $req) {
        try {
            $data = DB::table('asset_list')->where('asset_id', $req->id)->first();
            $response = [];
            if (!is_null($data) && ( !is_null($data->image) || $data->image != '' )) {
                $response['src'] = asset('images/'.$data->image);
                $response['title'] = $data->item_code;
                $response['code'] = 0;
            } else {
                $response['title'] = null;
                $response['code'] = 99;
                $response['msg'] = 'Data not found';
            }

            return $response;
        } catch (\Exception $e) {
            return [
                'code' => 99,
                'src' => null,
                'title' => 'Error',
                'msg' => $e->getMessage()
            ];
        }
    }

    public function printLabel(Request $req) {
        try {
            $export = $req->input('export');
            $arrAssetId = $req->input('asset_code');

            if (!$req->has('asset_code')) {
                return redirect()->back()->withErrors([
                    'error' => 'Please select data first'
                ]);
            }
            if ($export == 'label') {
                $arrData = [];
                for ($i=0; $i < count($arrAssetId); $i++) { 
                    $arrData[] = [
                        'code' => $arrAssetId[$i],
                        'qrcode' => QrCode::size(75)->generate(url('asset-detail/'. $arrAssetId[$i]))
                    ];
                }
                $param = [
                    'data' => array_chunk($arrData, 4)
                ];
    
                return view('asset-operation.asset-list.print-label', $param);
            } else {
                return Excel::download(new AssetListExport($arrAssetId), 'Asset_List-Export.xlsx');
            }
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function detail(Request $req) {
        try {
            $data = DB::table('v_asset_list')->where('asset_id', $req->id)->first();
            $movement = DB::table('v_asset_movement')->where('asset_id', $req->id)->get();
            $maintenance = DB::table('v_asset_maintenance')->where('asset_id', $req->id)->get();
            $param = [
                'title' => 'Asset Operation > Asset List > Detail - '.$data->asset_code,
                'data' => $data,
                'movement' => $movement,
                'maintenance' => $maintenance,
                'm_asset' => DB::table('m_asset')->where('deleted', 0)->get(),
                'location' => DB::table('location')->where('deleted', 0)->get(),
                'entity' => DB::table('entity')->where('deleted', 0)->get(),
                'supplier' => DB::table('supplier')->where('deleted', 0)->get(),
                'users' => DB::table('users')->where('deleted', 0)->get()
            ];
            return view('asset-operation.asset-list.detail', $param);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors([
                'error' => $e->getMessage()
            ])->withInput();
        }
    }

    public function detailEdit(Request $req) {
        try {
            $input = $req->input();
            $tab = '';
            if ($req->has('tab')) {
                unset($input['tab']);
                $tab = '?tab='.$req->get('tab');
            }

            if ($req->get('tab') == 'general') {
                if($req->image != '' || !is_null($req->image)){
                    $rules = [
                        'image' => 'mimes:jpg,png,jpeg|max:2048',
                    ];
                    $validator = Validator::make($req->file(), $rules);
                    if ($validator->fails()) {
                        return redirect()->back()->withErrors($validator->errors())->withInput();
                    }
                    
                    $path = public_path().'/images';
                    $data = DB::table('asset_list')->where('asset_id', $req->id)->first();
                    if($data->image != ''  && $data->image != null){
                        $file_old = $path.'/'.$data->image;
                        unlink($file_old);
                    }
                    
                    $file = $req->image;
                    $ext  = $file->getClientOriginalExtension();
                    $filename = $data->asset_id.'-'.$data->item_code.'.'.$ext;
                    $file->move($path, $filename);
                    $input['image'] = $filename;
                } else {
                    unset($input['image']);
                }
            }

            if ($req->get('tab') == 'other') {
                if($req->document != '' || !is_null($req->document)){
                    $rules = [
                        'document' => 'mimes:pdf,xls,xlsx,doc,docx|max:2048',
                    ];
                    $validator = Validator::make($req->file(), $rules);
                    if ($validator->fails()) {
                        return redirect()->back()->withErrors($validator->errors())->withInput();
                    }
                    
                    $path = public_path().'/documents';
                    $data = DB::table('asset_list')->where('asset_id', $req->id)->first();
                    if($data->document != ''  && $data->document != null){
                        $file_old = $path.'/'.$data->document;
                        unlink($file_old);
                    }
                    
                    $file = $req->document;
                    $ext  = $file->getClientOriginalExtension();
                    $filename = $data->asset_id.'-'.$data->item_code.'_doc.'.$ext;
                    $file->move($path, $filename);
                    $input['document'] = $filename;
                } else {
                    unset($input['document']);
                }
            }

            if ($req->get('tab') == 'info') {
                $input['purchase_cost'] = str_replace(',', '', $req->purchase_cost);
                $input['residual_value'] = str_replace(',', '', $req->residual_value);
            }
            unset($input['_token']);
            DB::table('asset_list')->where('asset_id', $req->id)->update($input);
            return redirect('asset-operation/asset-list/detail/'.$req->id.$tab)->withErrors([
                'success' => 'Data has been saved successfully'
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors([
                'error' => $e->getMessage()
            ])->withInput();
        }
    }

    public function deleteDocument(Request $req) {
        try {
            $path = public_path().'/documents';
            $data = DB::table('asset_list')->where('asset_id', $req->id)->first();
            if($data->document != ''  && $data->document != null){
                $file_old = $path.'/'.$data->document;
                unlink($file_old);
            } else {
                return redirect()->back()->withErrors([
                    'error' => 'Document not found'
                ])->withInput();
            }
            DB::table('asset_list')->where('asset_id', $req->id)->update(['document' => null]);
            return redirect('asset-operation/asset-list/detail/'.$req->id.'?tab=other')->withErrors([
                'success' => 'Data has been saved successfully'
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors([
                'error' => $e->getMessage()
            ])->withInput();
        }
    }

    public function ajaxGet(Request $req) {
        try {
            $data = DB::table('v_asset_list')
                ->where('asset_id', $req->asset_id)->first();
            if (is_null($data)) {
                $return = [
                    'code' => 99,
                    'data' => [],
                    'msg' => 'Data Not Found'
                ];
            } else {
                $return = [
                    'code' => 0,
                    'data' => $data,
                    'msg' => 'Success'
                ];
            }

            return $return;
        } catch (\Exception $e) {
            return [
                'code' => 99,
                'data' => [],
                'msg' => $e->getMessage()
            ];
        }
    }
}
