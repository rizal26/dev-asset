<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style>
        .bootstrap-btn {
            display: inline-block;
            font-weight: 400;
            color: #212529;
            text-align: center;
            border: 1px solid transparent;
            padding: .375rem .75rem;
            font-size: 1rem;
            line-height: 1.5;
            border-radius: .25rem;
            color: #fff;
            background-color: #007bff;
            text-decoration: none;
        }


        .bootstrap-btn:hover{
            background-color: #0069d9;

        }
    </style>
    <title>Purchase</title>
  </head>
  <body>
    <div style="margin: 30px; font-family: Arial, Helvetica, sans-serif;">
        <p><b>Dear {{ $mailto }},</b></p>
        <?=$content?>

        <p>This following is the asset information :</p>
        <table>
            <tbody>
                <tr>
                    <td><b>Request Number</b></td>
                    <td style="padding-left: 20px;padding-right: 20px;">:</td>
                    <td>PR-{{ str_pad($req_num, 5, '0', STR_PAD_LEFT) }}</td>
                </tr>
                <tr>
                    <td><b>Item Name</b></td>
                    <td style="padding-left: 20px;padding-right: 20px;">:</td>
                    <td>{{ $item_name }}</td>
                </tr>
                <tr>
                    <td><b>Brand</b></td>
                    <td style="padding-left: 20px;padding-right: 20px;">:</td>
                    <td>{{ $brand }}</td>
                </tr>
                <tr>
                    <td><b>Specification</b></td>
                    <td style="padding-left: 20px;padding-right: 20px;">:</td>
                    <td>{{ $spec }}</td>
                </tr>
                <tr>
                    <td><b>Category</b></td>
                    <td style="padding-left: 20px;padding-right: 20px;">:</td>
                    <td>{{ $category_name }}</td>
                </tr>
                <tr>
                    <td><b>Quantity</b></td>
                    <td style="padding-left: 20px;padding-right: 20px;">:</td>
                    <td>{{ $qty.' '.$uom_name }}</td>
                </tr>
                <tr>
                    <td><b>Expected Date</b></td>
                    <td style="padding-left: 20px;padding-right: 20px;">:</td>
                    <td>{{ date('d-m-Y', strtotime($expected_date)) }}</td>
                </tr>
                <tr>
                    <td><b>Remarks</b></td>
                    <td style="padding-left: 20px;padding-right: 20px;">:</td>
                    <td>{{ $remarks }}</td>
                </tr>
            </tbody>
        </table>
        <br>
        <p>For more information please login to your account, or click the button below</p>
        <a href="{{ route('login') }}" class="bootstrap-btn" target="_blank">Login</a>
        <p><b>Best Regards,<br>
        Admin Mitra Berlian Unggas</b><br>
        Jl. Mayang Padmi Kulon No. 31A<br>
        Kota Baru Parahyangan<br>
        Bandung, Jawa Barat<br>
        Indonesia 40553<br>
        +62 22 1000 189
        </p>
    </div>
  </body>
</html>

