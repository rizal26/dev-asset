<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/bootstrap-extended.css')}}">
    <style>
        .col-3 {
            padding-left: 7.5px;
            padding-right: 7.5px;
        }
        .card {
            width: 264.5px;
            height: 132.2px;
        }
        @page {
            size: A3;
            margin: 0;
        }
       
    </style>
    <title>Dev - Print Label</title>
</head>
<body>
    @for ($i = 0; $i < count($data); $i++)
    <div class="row" style="
        margin-top: 7.5px;
        margin-bottom: -24px;
        margin-right: 0;
        margin-left: 0;
    ">
        @for ($j = 0; $j < count($data[$i]); $j++)
            <div class="col-3">
                <div class="card" style="border: 1px solid;">
                    <div class="card-body" style="padding: 0.5rem">
                        <div class="row">
                            <div class="col-6" style="border-right: 1px solid #333; padding-top: 10px;">
                                <div class="text-center">
                                    {{ $data[$i][$j]['qrcode'] }}
                                    <span style="font-size: 9.5px"><b>{{ $data[$i][$j]['code'] }}</b></span>
                                </div>
                            </div>
                            <div class="col-6" style="line-height: 1;">
                                <center>
                                    <img src="{{ asset('assets/images/logo-dishub.png') }}" width="35"><br>
                                    <span style="font-size: 10px">Property of</span><br>
                                    <span style="font-size: 12px"><b>Dinas Perhubungan</b></span><br>
                                    <span style="font-size: 10px">DO NOT REMOVE<br>THIS LABEL</span>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endfor
    </div>
    @endfor

    <script src="{{asset('app-assets/vendors/js/vendors.min.js')}}"></script>
    <script>
        $(function () {
            window.print()
        });
    </script>
</body>
</html>