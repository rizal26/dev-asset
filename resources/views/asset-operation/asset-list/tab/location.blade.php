                                        <div class="tab-pane {{ $locationLink=='active'?'active':'fade' }}" id="account-vertical-location" role="tabpanel" aria-labelledby="account-pill-location" aria-expanded="{{ $locationAriaExpand }}">
                                            <!-- form -->
                                            <form class="validate-form" action="{{ route('asset-operation.asset-list.detail.edit', $data->asset_id) }}?tab=location" method="POST">
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-username">District</label>
                                                            <select name="location_id" class="form-control select2" id="location_id" {{ $disabled }}>
                                                                <option></option>
                                                                @foreach ($location as $item)
                                                                    <option value="{{ $item->location_id }}" 
                                                                        {{ $item->location_id==$data->location_id?'selected':'' }}>
                                                                        {{ $item->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Location</label>
                                                            <select name="building_id" id="building_id" class="form-control" {{ $disabled }}>
                                                                <option></option>
                                                                <option value="{{ $data->building_id }}" selected>
                                                                    {{ $data->building_name }}
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Room / Area</label>
                                                            <select name="area_id" id="area_id" class="form-control" {{ $disabled }}>
                                                                <option></option>
                                                                <option value="{{ $data->area_id }}" selected>
                                                                    {{ $data->area_name }}
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Floor</label>
                                                            <input type="text" name="floor" id="floor" class="form-control" value="{{ $data->floor }}" readonly/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">User / PIC</label>
                                                            <input type="text" class="form-control" name="assigned_user" value="{{ $data->assigned_user }}" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Entity</label>
                                                            <select name="entity_id" class="form_control select2" {{ $disabled }}>
                                                                <option></option>
                                                                @foreach ($entity as $key => $item)
                                                                    <option value="{{ $item->entity_id }}" 
                                                                        {{ $data->entity_id==$item->entity_id?'selected':'' }}>
                                                                        {{ $item->name}}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Department</label>
                                                            <select name="department_id" id="department_id" class="form-control" {{ $disabled }}>
                                                                <option></option>
                                                                <option value="{{ $data->department_id }}" selected>
                                                                    {{ $data->department_name }}
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-12">
                                                        @include('asset-operation.asset-list.tab.button')
                                                    </div>
                                                </div>
                                            </form>
                                            <!--/ form -->
                                        </div>