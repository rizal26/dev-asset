<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class TodoController extends Controller
{
    public function __construct() {
        $this->middleware(function ($request, $next) {
            if (!session('login')) {
                return redirect()->route('login')->send();
                die();
            };
            return $next($request);
        });
    }
    
    public function index() {
        try {
            $data = [
                'title' => 'Things To Do'
            ];
            $role = session('users')->role;
            $data['todo_purchase'] = $this->purchaseTodo($role);
            $data['todo_maintenance'] = $this->maintenanceTodo($role);
            $data['todo_movement'] = $this->movementTodo($role);

            return view('todo.index', $data);
        } catch (\Exception $e) {
            return abort(500, $e);
        }
    }

    public function todoCount(Request $req) {
        $data = $this->index();
        $count = count($data->todo_purchase)+count($data->todo_maintenance)+count($data->todo_movement);

        return $count;
    }

    function purchaseTodo($role) {
        $wherePurchase = ['deleted' => 0, 'status' => 0];
        if (in_array($role, [2,3])) {
            $wherePurchase['location_id'] = session('users')->location_id;
        }
        
        $dataPurchase = DB::table('v_asset_purchase')
            ->where($wherePurchase)
            ->get();

        $todoPurchase = [];
        foreach ($dataPurchase as $key => $value) {
            if ($value->order_status == 1 && $role == 3) {
                $todoPurchase[] = [
                    'request_id' => $value->request_id, 
                    'request_code' => $value->request_code
                ];
            } 
            
            if ($value->order_status == 2 && $role == 7) {
                $todoPurchase[] = [
                    'request_id' => $value->request_id, 
                    'request_code' => $value->request_code
                ];
            } 
            
            if ($value->order_status == 3 && $role == 4) {
                $todoPurchase[] = [
                    'request_id' => $value->request_id, 
                    'request_code' => $value->request_code
                ];
            } 
            
            if ($value->order_status == 4 && $role == 5) {
                $todoPurchase[] = [
                    'request_id' => $value->request_id, 
                    'request_code' => $value->request_code
                ];
            }
            
            if ($value->order_status == 5 && $role == 6) {
                $todoPurchase[] = [
                    'request_id' => $value->request_id, 
                    'request_code' => $value->request_code
                ];
            } 
            
            if ($value->order_status == 6 && $value->created_by == session('users')->user_id) {
                $todoPurchase[] = [
                    'request_id' => $value->request_id, 
                    'request_code' => $value->request_code
                ];
            } 
            
            if ($value->order_status == 8 && $role == 4) {
                $todoPurchase[] = [
                    'request_id' => $value->request_id, 
                    'request_code' => $value->request_code
                ];
            }

            if ($value->order_status == 9 && $role == 7) {
                $todoPurchase[] = [
                    'request_id' => $value->request_id, 
                    'request_code' => $value->request_code
                ];
            }

            if ($value->order_status == 10 && $role == 7 && $value->validate == 0) {
                $todoPurchase[] = [
                    'request_id' => $value->request_id, 
                    'request_code' => $value->request_code
                ];
            }
        }
        return $todoPurchase;
    }

    function maintenanceTodo($role) {
        $where = [

            'deleted' => 0, 
            'status' => 0
        ];
        if (in_array($role, [2,3])) {
            $where['location_id'] = session('users')->location_id;
        }
        
        $data = DB::table('v_asset_maintenance')
            ->where($where)
            ->get();

        $todo = [];
        foreach ($data as $key => $value) {
            if ($role == 3) {
                $todo[] = [
                    'maintenance_id' => $value->maintenance_id, 
                    'maintenance_code' => $value->maintenance_code
                ];
            } 
            
        }
        return $todo;
    }

    function movementTodo($role) {
        $where = [
            'deleted' => 0
        ];

        $data = DB::table('v_asset_movement')
            ->where($where)
            ->get();
        
        $todo = [];
        foreach ($data as $key => $value) {
            if ($role == 3 && $value->status == 0) {
                $todo[] = [
                    'movement_id' => $value->movement_id, 
                    'movement_code' => $value->movement_code
                ];
            } 

            if ($role == 7 && $value->status == 1 && $value->validate == 0) {
                $todo[] = [
                    'movement_id' => $value->movement_id, 
                    'movement_code' => $value->movement_code
                ];
            } 
            
        }

        return $todo;
    }
}
