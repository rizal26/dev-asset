@extends('templates.main')
@section('title', $title)
@section('content')
            
                @include('templates.message-validation')
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{$title}}</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form form-horizontal" method="post" action="{{ route('data-master.asset.edit', $data->m_asset_id) }}">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="code" class="float-right">Category</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <select name="category_id" class="form-control">
                                                            <option></option>
                                                            @foreach ($category as $item)
                                                                <option value="{{ $item->category_id }}" 
                                                                    {{ $item->category_id==$data->category_id?'selected':'' }}>{{ $item->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="name" class="float-right">Asset Name</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="text" id="name" class="form-control" name="nama" placeholder="Name" value="{{ $data->nama }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-9 offset-sm-3">
                                                <button type="submit" class="btn btn-primary mr-1 waves-effect waves-float waves-light">update</button>
                                                <a href="{{ route('data-master.asset.index') }}" class="btn btn-outline-warning waves-effect">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script src="{{asset('app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
                    <script>
                        $(function () {
                            $('select').select2({
                                placeholder: 'Select',
                                allowClear: true
                            });
                        });
                    </script>
@endsection