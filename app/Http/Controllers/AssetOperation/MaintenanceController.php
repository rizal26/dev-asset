<?php

namespace App\Http\Controllers\AssetOperation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Mail\MaintenanceAssetMgt;
use DB;

class MaintenanceController extends Controller
{
    public function __construct() {
        $this->middleware(function ($request, $next) {
            if (!session('login')) {
                return redirect()->route('login')->withErrors(['error' => 'Please sign in to continue']);
                die();
            };
            return $next($request);
        });
    }

    public function index(Request $req) {
        try {
            $param = [
                'title' => 'Asset Maintenace'  
            ];

            return view('asset-operation.asset-maintenance.index', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function ajax(Request $req) {
        $where = ['deleted' => 0];
        if (in_array(session('users')->role, [2, 3])) {
            $where['location_id'] = session('users')->location_id;
            $where['department_id'] = session('users')->department_id;
        }
        $data = DB::table('v_asset_maintenance')->where($where);
        $datatable = Datatables::of($data)
            ->editColumn('created_date', function($data) {
                return date('d-M-Y', strtotime($data->created_date));
            })
            ->editColumn('status', function($data) {
                $return = 'Undifined';
                if ($data->status == 0) {
                    $return = '<div class="badge badge-pill badge-glow badge-warning">Waiting for Approval</div>';
                } elseif ($data->status == 1) {
                    $return = '<div class="badge badge-pill badge-glow badge-success">Approved</div>';
                } else {
                    $return = '<div class="badge badge-pill badge-glow badge-danger">Rejected</div>';
                }
                return $return;
            })
            ->addColumn('action', function($data) {
                $button = '<a href="'.route("asset-operation.asset-maintenance.detail", $data->maintenance_id).'" class="btn btn-icon btn-flat-primary waves-effect" title="View Detail"><i data-feather="alert-circle"></i></a>';
                return $button;                
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
        return $datatable;
    }

    public function detail(Request $req) {
        try {
            $maintenance_id = $req->id;
            $data = DB::table('v_asset_maintenance')
                ->where('maintenance_id', $maintenance_id)
                ->first();
            $param = [
                'title' => 'Asset Maintenace > Detail',
                'data' => $data
            ];

            return view('asset-operation.asset-maintenance.detail', $param);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors([
                'error' => $e->getMessage()
            ])->withInput();
        }
    }

    public function add(Request $req) {
        try {
            $vendors = DB::table('supplier')->get();
            $param = [
                'title' => 'Asset Maintenace > Add',
                'vendors' => $vendors
            ];

            if ($req->isMethod('post')) {
                $insert = $req->input();
                unset($insert['_token']);
                $insert['total_cost'] = is_null($insert['total_cost'])?null:intval(\str_replace(',', '', $insert['total_cost']));
                $messages = [
                    'asset_id.required' => 'Asset Code field is required',
                    'total_cost.required' => 'Total Cost field is required',
                    'supplier_id.required' => 'Vendor field is required'
                ];

                $rules = [
                    'asset_id' => 'required',
                    'total_cost' => 'required',
                    'supplier_id' => 'required'
                ];
                $validator = Validator::make($insert, $rules, $messages);
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput();
                }

                $insert['created_by'] = session('users')->user_id;
                $mvId = DB::table('asset_maintenance')->insertGetId($insert);
                $data = DB::table('v_asset_maintenance')->where('maintenance_id', $mvId)->first();
                $asset = DB::table('asset_list')->where('asset_id', $data->asset_id)->first();

                $insert['creator_name'] = $data->creator_name;
                $insert['asset_code'] = $data->asset_code;
                $insert['asset_name'] = $data->asset_name;
                $insert['content'] = '<p>There is an asset maintenance request from <b>'. $data->creator_name .'</b> that require your validation.</p>';

                $assetMgt = DB::table('users')
                    ->where('role', 3)
                    ->where('location_id', $asset->location_id)
                    ->get();

                foreach ($assetMgt as $key => $value) {
                    $insert['mailto'] = $value->name;
                    Mail::to($value->email)->send(new MaintenanceAssetMgt($insert));
                }
                $success = ['success' => 'Data has been saved successfuly'];
                return redirect()->route('asset-operation.asset-maintenance.index')->withErrors($success);
            }

            return view('asset-operation.asset-maintenance.add', $param);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors([
                'error' => $e->getMessage()
            ])->withInput();
        }
    }

    public function approve(Request $req) {
        try {
            $maintenanceId = $req->id;
            $data = DB::table('v_asset_maintenance')
                ->where('maintenance_id', $maintenanceId)
                ->first();
            $users = DB::table('users')->where('user_id', $data->created_by)->first();
            $insert = [
                'created_by' => $data->creator_name,
                'asset_code' => $data->asset_code,
                'asset_name' => $data->asset_name,
                'total_cost' => $data->total_cost,
                'currency' => $data->currency,
                'mailto' => $users->name,
                'content' => '<p>Congratulation, Your maintenance asset has been approved</p>'
            ];

            Mail::to($users->email)->send(new MaintenanceAssetMgt($insert));

            DB::table('asset_maintenance')->where('maintenance_id', $maintenanceId)->update([
                'status' => 1,
                'approved_by' => session('users')->user_id
            ]);
            return redirect()->back()->withErrors([
                'success' => 'asset has been approved successfully'
            ])->withInput();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors([
                'error' => $e->getMessage()
            ])->withInput();
        }
    }

    public function reject(Request $req) {
        try {
            $maintenanceId = $req->id;
            $data = DB::table('v_asset_maintenance')
                ->where('maintenance_id', $maintenanceId)
                ->first();
            $users = DB::table('users')->where('user_id', $data->created_by)->first();
            $insert = [
                'created_by' => $data->creator_name,
                'asset_code' => $data->asset_code,
                'asset_name' => $data->asset_name,
                'total_cost' => $data->total_cost,
                'currency' => $data->currency,
                'mailto' => $users->name,
                'content' => '<p>Oh no, Your maintenance asset has been rejected</p><p>Reason : '.$req->reason
            ];

            Mail::to($users->email)->send(new MaintenanceAssetMgt($insert));

            DB::table('asset_maintenance')->where('maintenance_id', $maintenanceId)->update([
                'status' => 2,
                'reason_rejected' =>  $req->reason,
                'approved_by' => session('users')->user_id
            ]);
            return redirect()->back()->withErrors([
                'success' => 'asset has been rejected successfully'
            ])->withInput();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors([
                'error' => $e->getMessage()
            ])->withInput();
        }
    }
}
