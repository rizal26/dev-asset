@extends('templates.main')
@section('title', $title)
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.css">

            @include('asset-purchase.wizard')
                @if($errors->any())
                    @foreach($errors->all() as $error)
                        @if($errors->has('success'))
                        <div class="alert alert-success">
                            <div class="alert-body">
                                <strong>{{ $error }}</strong>
                            </div>
                        </div>
                        @else
                        <div class="alert alert-danger">
                            <div class="alert-body">
                                <strong>{{ $error }}</strong>
                            </div>
                        </div>
                        @endif    
                    @endforeach
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">{{ $title }}</h4>
                            </div>
                            <div class="card-body">
                                <form class="form form-horizontal" method="post" action="{{ route('asset-purchase.step1') }}">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="brand">Brand</label>
                                            <input type="text" class="form-control" name="brand" placeholder="Brand" value="{{ old('brand') }}" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="m_asset_id">Item Name</label>
                                            <select name="m_asset_id" id="m_asset_id" class="form-control select2" required>
                                                <option></option>
                                                @foreach ($m_asset as $item)
                                                <option value="{{ $item->m_asset_id }}" {{ old('m_asset_id')==$item->m_asset_id?'selected':'' }}>{{ $item->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="category">Category</label>
                                            <input type="text" id="category" class="form-control" placeholder="Category" readonly required />
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="spec">Specification</label>
                                            <input type="text" name="spec" id="spec" class="form-control" placeholder="Specification" value="{{ old('spec') }}" required/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="qty">Quantity</label>
                                            <input type="number" name="qty" class="form-control" placeholder="Quantity" value="{{ old('qty') }}" required/>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="uom">UOM</label>
                                            <select class="select2 form-control w-100" name="uom" id="uom" required>
                                                <option></option>
                                                @foreach ($uom as $item)
                                                    <option value="{{ $item->uom_id }}" {{ old('uom_id')==$item->uom_id?'selected':'' }}>{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="expected_date">Expected Date</label>
                                            <input type="text" class="form-control flatpickr-basic" placeholder="" name="expected_date" value="{{ old('expected_date') }}" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="priority">Priority Level</label>
                                            <select name="priority" class="form-control select2" required>
                                                <option></option>
                                                <option value="1">High</option>
                                                <option value="2">Medium</option>
                                                <option value="3">Low</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="remarks">Remarks</label>
                                            <textarea name="remarks" cols="30" class="form-control" placeholder="Remarks" required></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-primary mr-1 waves-effect waves-float waves-light">Submit</button>
                                            <a href="{{ route('asset-purchase.index') }}" class="btn btn-outline-warning waves-effect">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <script src="{{asset('app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
                <script src="{{asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js')}}"></script>
                <script>
                    $(function () {
                        $('select[name=m_asset_id]').trigger("change");
                        var basicPickr = $('.flatpickr-basic');
                        if (basicPickr.length) {
                            basicPickr.flatpickr();
                        }
                        $('.select2').select2({
                            placeholder: 'Select',
                            allowClear: true
                        });

                         $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $('#m_asset_id').change(function (e) { 
                            e.preventDefault();
                            console.log($(this).val());
                            if ($(this).val() == '') {
                                $('#category').val('');
                            } else {
                                $.ajax({
                                    type: 'post',
                                    url: "{{ route('select.asset') }}",
                                    data: { m_asset_id: $(this).val() },
                                    success: function (response) {
                                        $('input#category').val(response.data);
                                    }
                                });
                            }
                        });
                    });
                </script>
@endsection