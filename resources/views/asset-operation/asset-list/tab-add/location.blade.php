                                        <div class="tab-pane {{ $locationLink=='active'?'active':'fade' }}" id="account-vertical-location" role="tabpanel" aria-labelledby="account-pill-location" aria-expanded="{{ $locationAriaExpand }}">
                                            <!-- form -->
                                            <form class="validate-form" action="{{ route('asset-operation.asset-list.add') }}?tab=location" method="POST">
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-username">District</label>
                                                            <select name="location_id" class="form-control select2" id="location_id" {{ $disabled }}>
                                                                <option></option>
                                                                @foreach ($location as $item)
                                                                    <option value="{{ $item->location_id }}" 
                                                                        {{ $item->location_id==old('location_id')?'selected':'' }}>
                                                                        {{ $item->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Location</label>
                                                            <select name="building_id" id="building_id" class="form-control" {{ $disabled }}>
                                                                <option></option>
                                                                <option value="{{ old('building_id') }}" selected>
                                                                    {{ old('building_name') }}
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Room / Area</label>
                                                            <select name="area_id" id="area_id" class="form-control" {{ $disabled }}>
                                                                <option></option>
                                                                <option value="{{ old('area_id') }}" selected>
                                                                    {{ old('area_name') }}
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Floor</label>
                                                            <input type="text" name="floor" id="floor" class="form-control" value="{{ old('floor') }}" readonly/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">User / PIC</label>
                                                            <input type="text" class="form-control" name="assigned_user" value="{{ old('assigned_user') }}" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Entity</label>
                                                            <select name="entity_id" class="form_control select2" {{ $disabled }}>
                                                                <option></option>
                                                                @foreach ($entity as $key => $item)
                                                                    <option value="{{ $item->entity_id }}" 
                                                                        {{ old('entity_id')==$item->entity_id?'selected':'' }}>
                                                                        {{ $item->name}}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Department</label>
                                                            <select name="department_id" id="department_id" class="form-control" {{ $disabled }}>
                                                                <option></option>
                                                                <option value="{{ old('department_id') }}" selected>
                                                                    {{ old('department_name') }}
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-12">
                                                        <button type="button" id="nextFromLocation" class="btn btn-primary mt-1 mr-1">Next</button>
                                                        <a href="{{ route('asset-operation.asset-list.index') }}" class="btn btn-outline-secondary mt-2">Cancel</a>
                                                    </div>
                                                </div>
                                            </form>
                                            <!--/ form -->
                                        </div>

                                        <script>
                                            $(function () {
                                                $('#nextFromLocation').click(function (e) { 
                                                    e.preventDefault();
                                                    $('#account-vertical-info').addClass('active');                                                    
                                                    $('#account-vertical-info').addClass('fade');                                                    
                                                    $('#account-vertical-info').addClass('show');                                                    
                                                    $('#account-vertical-info').attr('aria-expanded', 'true');
                                                    $('#account-pill-info').addClass('active');                                                    
                                                    $('#account-pill-info').attr('aria-expanded', 'true');
    
                                                    $('#account-vertical-location').removeClass('active');                                                    
                                                    $('#account-vertical-location').removeClass('show');                                                    
                                                    $('#account-vertical-location').removeClass('fade');                                                    
                                                    $('#account-vertical-location').attr('aria-expanded', 'false');                                                    
                                                    $('#account-pill-location').removeClass('active');                                                    
                                                    $('#account-pill-location').attr('aria-expanded', 'false'); 
                                                });
                                            });
                                        </script>