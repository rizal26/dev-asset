        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="{{ url('/') }}" style="margin-top: 0.35rem;"><span class="brand-logo">
                        <img src="{{ asset('app-assets/images/logo/logo.png') }}" alt="" style="max-width: 40px;"> </span>
                        <h2 class="brand-text">DISHUB</h2>
                    </a></li>
                <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        
        <input type="hidden" id="url" value="{{ Request::path() }}">
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li id="todo" class=" nav-item"><a class="d-flex align-items-center" href="{{ route('todo') }}"><i data-feather="target"></i><span class="menu-title text-truncate" data-i18n="Things To Do">Things To Do</span><span class="badge badge-light-warning badge-pill ml-auto mr-1" id="todoCount"></span></a>
                </li>
                <li id="home" class=" nav-item"><a class="d-flex align-items-center" href="{{ route('home') }}"><i data-feather="home"></i><span class="menu-title text-truncate" data-i18n="Dashboard">Dashboard</span></a>
                </li>
                <li class=" navigation-header"><span data-i18n="Features">Features</span><i data-feather="more-horizontal"></i>
                </li>
                <li id="asset-purchase" class=" nav-item"><a class="d-flex align-items-center" href="{{ route('asset-purchase.index') }}"><i data-feather="shopping-cart"></i><span class="menu-title text-truncate" data-i18n="Purchase Request">Purchase Request</span></a>
                </li>
                <li class=" nav-item has-sub {{ Request::segment(1)=='asset-operation'?'sidebar-group-active open':'' }}" ><a class="d-flex align-items-center" href="#"><i data-feather="layers"></i><span class="menu-title text-truncate" data-i18n="Asset Operation">Asset Operation</span></a>
                    <ul class="menu-content">
                        <li id="asset-list"><a class="d-flex align-items-center" href="{{ route('asset-operation.asset-list.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Asset List">Asset List</span></a>
                        </li>
                        <li id="asset-maintenance"><a class="d-flex align-items-center" href="{{ route('asset-operation.asset-maintenance.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Asset Maintenance">Asset Maintenance</span></a>
                        </li>
                        <li id="asset-movement"><a class="d-flex align-items-center" href="{{ route('asset-operation.asset-movement.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Asset Movement">Asset Movement</span></a>
                        </li>
                    </ul>
                </li>
                @if ( in_array(session('users')->role, [1, 7]))
                <li class=" nav-item has-sub {{ Request::segment(1)=='data-master'?'sidebar-group-active open':'' }}"><a class="d-flex align-items-center" href="#"><i data-feather="settings"></i><span class="menu-title text-truncate" data-i18n="Data Master">Data Master</span></a>
                    <ul class="menu-content">
                        <li id="asset"><a class="d-flex align-items-center" href="{{ route('data-master.asset.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Asset">Asset</span></a>
                        </li>
                        <li id="category"><a class="d-flex align-items-center" href="{{ route('data-master.category.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Category">Category</span></a>
                        </li>
                        <li id="entity"><a class="d-flex align-items-center" href="{{ route('data-master.entity.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Entity">Entity</span></a>
                        </li>
                        <li id="department"><a class="d-flex align-items-center" href="{{ route('data-master.department.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Department">Department</span></a>
                        </li>
                        <li id="district"><a class="d-flex align-items-center" href="{{ route('data-master.district.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="district">District</span></a>
                        </li>
                        <li id="location"><a class="d-flex align-items-center" href="{{ route('data-master.location.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="location">Location</span></a>
                        </li>
                        <li id="area"><a class="d-flex align-items-center" href="{{ route('data-master.area.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Area">Area</span></a>
                        </li>
                        <li id="supplier"><a class="d-flex align-items-center" href="{{ route('data-master.supplier.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Supplier">Supplier</span></a>
                        </li>
                    </ul>
                </li>
                @endif
                <li id="users" class="nav-item"><a class="d-flex align-items-center" href="{{ route('users.index') }}"><i data-feather="user"></i><span class="menu-title text-truncate" data-i18n="Users">Users</span></a>
                </li>
            </ul>
        </div>

        <script>
            $(function () {
                var urlPath = $('#url').val(); 
                var arrPath = urlPath.split('/');
                $('#'+arrPath[0]).addClass('active');
                $('#'+arrPath[1]).addClass('active');

                $.ajax({
                    type: "get",
                    url: "{{ route('todo.count') }}",
                    success: function (response) {
                        $('#todoCount').html(response);
                    }
                });
            });
        </script>