                        <!-- left menu section -->
                        <div class="col-md-3 mb-2 mb-md-0">
                            <ul class="nav nav-pills flex-column nav-left">
                                <!-- general -->
                                <li class="nav-item">
                                    <a class="nav-link {{ $generalLink }}" id="account-pill-general" data-toggle="pill" href="#account-vertical-general" aria-expanded="{{ $generalAriaExpand }}">
                                        <i data-feather='globe' class="font-medium-3 mr-1"></i>
                                        <span class="font-weight-bold">General</span>
                                    </a>
                                </li>
                                <!-- location -->
                                <li class="nav-item">
                                    <a class="nav-link {{ $locationLink }}" id="account-pill-location" data-toggle="pill" href="#account-vertical-location" aria-expanded="{{ $locationAriaExpand }}">
                                        <i data-feather='user-check' class="font-medium-3 mr-1"></i>
                                        <span class="font-weight-bold">Location & User</span>
                                    </a>
                                </li>
                                <!-- information -->
                                <li class="nav-item">
                                    <a class="nav-link {{ $infoLink }}" id="account-pill-info" data-toggle="pill" href="#account-vertical-info" aria-expanded="{{ $infoAriaExpand }}">
                                        <i data-feather='dollar-sign' class="font-medium-3 mr-1"></i>
                                        <span class="font-weight-bold">Purchase Info</span>
                                    </a>
                                </li>
                                <!-- other information -->
                                <li class="nav-item">
                                    <a class="nav-link {{ $otherLink }}" id="account-pill-other" data-toggle="pill" href="#account-vertical-other" aria-expanded="{{ $otherAriaExpand }}">
                                        <i data-feather="info" class="font-medium-3 mr-1"></i>
                                        <span class="font-weight-bold">Other Information</span>
                                    </a>
                                </li>
                            </ul>
                            <div class="text-center">
                                @if (isset($data))
                                <img src="{{ asset('images/'.$data->image) }}" width="200" alt="">
                                @endif
                            </div>
                        </div>
                        <!--/ left menu section -->