@extends('templates.main')
@section('title', $title)
@section('content')

<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/charts/apexcharts.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/plugins/charts/chart-apex.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/pages/dashboard-ecommerce.css')}}">
    <!-- <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.4/dist/leaflet.css" />   -->
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/css/maps/leaflet.min.css') }}">  
    <div class="row match-height">
        <div class="col-xl-2 col-md-6 col-12">
            <div class="card">
                <div class="card-body pb-50">
                    <h6>Purchase Cost</h6>
                    <h2 class="font-weight-bolder mb-1" style="font-size: {{ strlen($totalPurchase)>=10?'20px':'' }}">{{ number_format($totalPurchase) }}</h2>
                    <div id="statistics-order-chart"></div>
                </div>
            </div>
        </div>
        <div class="col-xl-2 col-md-6 col-12">
            <div class="card card-tiny-line-stats">
                <div class="card-body pb-50">
                    <h6>Maintenance Cost</h6>
                    <h2 class="font-weight-bolder mb-1" style="font-size: {{ strlen($totalMt)>=10?'20px':'' }}">{{ number_format($totalMt) }}</h2>
                    <div id="statistics-profit-chart"></div>
                </div>
            </div>
        </div>
        <!-- Statistics Card -->
        <div class="col-xl-8 col-md-6 col-12">
            <div class="card card-statistics">
                <div class="card-header">
                    <h4 class="card-title">Asset Statistics</h4>
                </div>
                <div class="card-body statistics-body">
                    <div class="row">
                        <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                            <div class="media">
                                <div class="avatar bg-light-primary mr-2">
                                    <div class="avatar-content">
                                        <i data-feather='database' class="avatar-icon"></i>
                                    </div>
                                </div>
                                <div class="media-body my-auto">
                                    <h4 class="font-weight-bolder mb-0">{{ $totalAsset }}</h4>
                                    <p class="card-text font-small-3 mb-0">Total Asset</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                            <div class="media">
                                <div class="avatar bg-light-success mr-2">
                                    <div class="avatar-content">
                                        <i data-feather='check-circle' class="avatar-icon"></i>
                                    </div>
                                </div>
                                <div class="media-body my-auto">
                                    <h4 class="font-weight-bolder mb-0">{{ $totalGood }}</h4>
                                    <p class="card-text font-small-3 mb-0">Total Good Asset</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-sm-0">
                            <div class="media">
                                <div class="avatar bg-light-warning mr-2">
                                    <div class="avatar-content">
                                        <i data-feather='alert-circle' class="avatar-icon"></i>
                                    </div>
                                </div>
                                <div class="media-body my-auto">
                                    <h4 class="font-weight-bolder mb-0">{{ $totalLight }}</h4>
                                    <p class="card-text font-small-3 mb-0">Total Light Damage</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-sm-6 col-12">
                            <div class="media">
                                <div class="avatar bg-light-danger mr-2">
                                    <div class="avatar-content">
                                        <i data-feather='x-circle' class="avatar-icon"></i>
                                    </div>
                                </div>
                                <div class="media-body my-auto">
                                    <h4 class="font-weight-bolder mb-0">{{ $totalHeavy }}</h4>
                                    <p class="card-text font-small-3 mb-0">Total Heavy Damage</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ Statistics Card -->
    </div>
    <div class="row match-height">
        <div class="col-xl-6 col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                    <div class="header-left">
                        <h4 class="card-title">Asset Condition by Category</h4>
                    </div>
                </div>
                <div class="card-body">
                    <canvas class="chart-category chartjs1" data-height="400"></canvas>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                    <div class="header-left">
                        <h4 class="card-title">Asset Condition by Location</h4>
                    </div>
                </div>
                <div class="card-body">
                    <canvas class="chart-location chartjs2" data-height="400"></canvas>
                </div>
            </div>
        </div>
    </div>

    <div class="row match-height">
        
        <div class="col-xl-12 col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                    <div class="header-left">
                        <h4 class="card-title">Asset on Maps</h4>
                    </div>
                </div>
                <div class="card-body">
                    <div id="map" style = "width: 100%; height: 580px"></div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{asset('app-assets/vendors/js/charts/apexcharts.min.js')}}"></script>
    <script src="{{asset('app-assets/js/scripts/pages/dashboard-ecommerce.js')}}"></script>
    <script src="{{asset('app-assets/vendors/js/charts/chart.min.js')}}"></script>
    <!-- <script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js"></script> -->
    <script src="{{ asset('app-assets/vendors/js/maps/leaflet.min.js') }}"></script>
    
    @include('chart-category')
    @include('chart-location')
    <script>
        var map = L.map('map').setView([-6.593797,139.1312538], 7.3);
        L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        }).addTo(map);

        var markers = [
            {
                coordinates: [-8.517619, 140.416880],
                popupContent: "<img src='https://cdn-2.tstatic.net/papua/foto/bank/images/kantor-dinas-perhubungan-kabupaten-merauke.jpg' width='150'><br><br>"+
                                "<b>Nama Asset</b> : Gedung Dishub<br>"+
                                "<b>Nomor Asset</b> : 123456<br>"+
                                "<b>Kondisi</b> : Baik"
            },
            {
                coordinates: [-7.256218, 139.972957],
                popupContent: "<img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSSOK7E8k3XsLZtc2UFMbNNoM-QaIFAXs-I6A&usqp=CAU' width='150'><br><br>"+
                                "<b>Nama Asset</b> : Tanah<br>"+
                                "<b>Nomor Asset</b> : 123456<br>"+
                                "<b>Kondisi</b> : Baik"
            },
            {
                coordinates: [-8.155457, 138.251644],
                popupContent: "<img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTa4T4hgIFd77liidAuQRZFwoqf0WmRUaGNjA&usqp=CAU' width='150'><br><br>"+
                                "<b>Nama Asset</b> : Jalan Provinsi<br>"+
                                "<b>Nomor Asset</b> : 123456<br>"+
                                "<b>Kondisi</b> : Baik"
            },
            // Add more markers as needed
        ];
        for (var i = 0; i < markers.length; i++) {
            var marker = L.marker(markers[i].coordinates).addTo(map);
            marker.bindPopup(markers[i].popupContent);
        }
    </script>
    <script>
        $.ajax({
            type: "GET",
            url: "{{ route('home.chart.category') }}",
            success: chartByCategory,
        });
        $.ajax({
            type: "GET",
            url: "{{ route('home.chart.location') }}",
            success: chartByLocation,
        });
    </script>


@endsection