@extends('templates.main')
@section('title', $title)
@section('content')
    <h3><b>{{ $title }}</b></h3>
    <div class="divider divider-left mt-2">
        <div class="divider-text">Asset Purchase</div>
    </div>
    <div class="row" id="assetPurchase">
        @foreach ($todo_purchase as $item)
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title text-center">{{ $item['request_code'] }} Need Your Approval</h4>
                    <p class="card-text text-center">
                        Click on button below to do something<br>with this task
                    </p>
                    <div class="text-center">
                        <a href="{{ url('asset-purchase/detail/'.$item['request_id'] ) }}" class="btn btn-sm btn-primary">Do Something</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="divider divider-left mt-2">
        <div class="divider-text">Asset Maintenance</div>
    </div>
    <div class="row" id="assetMaintenance">
        @foreach ($todo_maintenance as $item)
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title text-center">{{ $item['maintenance_code'] }} Need Your Approval</h4>
                    <p class="card-text text-center">
                        Click on button below to do something<br>with this task
                    </p>
                    <div class="text-center">
                        <a href="{{ url('asset-operation/asset-maintenance/detail/'.$item['maintenance_id'] ) }}" class="btn btn-sm btn-primary">Do Something</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="divider divider-left mt-2">
        <div class="divider-text">Asset Movement</div>
    </div>
    <div class="row" id="assetMovement">
        @foreach ($todo_movement as $item)
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title text-center">{{ $item['movement_code'] }} Need Your Approval</h4>
                    <p class="card-text text-center">
                        Click on button below to do something<br>with this task
                    </p>
                    <div class="text-center">
                        <a href="{{ url('asset-operation/asset-movement/detail/'.$item['movement_id'] ) }}" class="btn btn-sm btn-primary">Do Something</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>

@endsection