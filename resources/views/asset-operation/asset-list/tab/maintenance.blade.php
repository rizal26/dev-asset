                                        <div class="tab-pane {{ $maintenanceLink=='active'?'active':'fade' }}" id="account-vertical-maintenance" role="tabpanel" aria-labelledby="account-pill-maintenance" aria-expanded="{{ $maintenanceAriaExpand }}">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table id="datatableMaintenance" class="table table-bordered table-striped">
                                                            <thead>
                                                                <th>ID</th>
                                                                <th>Date</th>
                                                                <th>Description</th>
                                                                <th>Total Cost</th>
                                                                <th>Vendor</th>
                                                                <th>Status</th>
                                                            </thead>
                                                            <tbody>
                                                                @php
                                                                    $sum = 0;
                                                                @endphp
                                                                @foreach ($maintenance as $item)
                                                                    @php
                                                                        $sum += $item->total_cost;
                                                                    @endphp
                                                                    <tr>
                                                                        <td>{{ $item->maintenance_id }}</td>
                                                                        <td>{{ date('d-M-Y', strtotime($item->created_date)) }}</td>
                                                                        <td>{{ $item->descr }}</td>
                                                                        <td>{{ $item->currency.' '.number_format($item->total_cost) }}</td>
                                                                        <td>{{ $item->vendor_name }}</td>
                                                                        <td>
                                                                            @php
                                                                                $status = 'Undifined';
                                                                                if ($item->status == 0) {
                                                                                    $status = '<div class="badge badge-pill badge-glow badge-warning">Waiting for Approval</div>';
                                                                                } elseif ($item->status == 1) {
                                                                                    $status = '<div class="badge badge-pill badge-glow badge-success">Approved</div>';
                                                                                } else {
                                                                                    $status = '<div class="badge badge-pill badge-glow badge-danger">Rejected</div>';
                                                                                }
                                                                            @endphp
                                                                            <?=$status?>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <p>Total maintenance cost: IDR {{ number_format($sum) }}</p>
                                                </div>
                                            </div>
                                        </div>