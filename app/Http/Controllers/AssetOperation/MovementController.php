<?php

namespace App\Http\Controllers\AssetOperation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Mail;
use App\Mail\Movement;
use App\Mail\MovementAssetMgt;
use DB;

class MovementController extends Controller
{
    public function __construct() {
        $this->middleware(function ($request, $next) {
            if (!session('login')) {
                return redirect()->route('login')->withErrors(['error' => 'Please sign in to continue']);
                die();
            };
            return $next($request);
        });
    }

    public function index(Request $req) {
        try {
            $param = [
                'title' => 'Asset Movement'  
            ];

            if(isset($_GET['resp'])) {
                if ($_GET['resp'] == 0) {
                    return redirect()->route('asset-operation.asset-movement.index')->withErrors([
                        'success' => 'Movement Data has been created successfully'
                    ]);
                } else {
                    return redirect()->route('asset-operation.asset-movement.index')->withErrors([
                        'error' => 'Something went wrong, please contact Administrator'
                    ]);
                }
            }

            return view('asset-operation.asset-movement.index', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function ajax(Request $req) {
        $data = DB::table('v_asset_movement');
        $datatable = Datatables::of($data)
            ->editColumn('created_date', function($data) {
                return date('d-M-Y', strtotime($data->created_date));
            })
            ->editColumn('status', function($data) {
                $return = 'Undifined';
                if ($data->status == 0) {
                    $return = '<div class="badge badge-pill badge-glow badge-warning">Waiting for approval</div>';
                } elseif ($data->status == 1) {
                    $return = '<div class="badge badge-pill badge-glow badge-success">Approved</div>';
                } else {
                    $return = '<div class="badge badge-pill badge-glow badge-danger">Rejected</div>';
                }
                return $return;
            })
            ->editColumn('validate', function($data) {
                $return = 'Undifined';
                if ($data->validate == 0) {
                    $return = '<div class="badge badge-pill badge-glow badge-warning">Waiting for validation</div>';
                } elseif ($data->validate == 1) {
                    $return = '<div class="badge badge-pill badge-glow badge-success">Approved</div>';
                } else {
                    $return = '<div class="badge badge-pill badge-glow badge-danger">Rejected</div>';
                }
                return $return;
            })
            ->addColumn('action', function($data) {
                $button = '<a href="'.route("asset-operation.asset-movement.detail", $data->movement_id).'" class="btn btn-icon btn-flat-primary waves-effect" title="View Detail"><i data-feather="alert-circle"></i></a>';
                return $button;                
            })
            ->rawColumns(['status', 'validate', 'action'])
            ->make(true);
        return $datatable;
    }

    public function add(Request $req) {
        try {
            $role = session('users')->role;
            if (!in_array($role, [1,2,3])) {
                return redirect()->back()->withErrors([
                    'error' => 'Sorry, you are not eligible to access this page'
                ])->withInput();
            }

            $locList = DB::table('district')->where('deleted', 0)->get();
            $users = DB::table('users')->where('deleted',0)->get();
            $param = [
                'title' => 'Asset Movement Add',
                'loclist' => $locList,
                'users' => $users,
            ];

            if (count($_GET) > 0) {
                if (!$_GET['prev_area_id'] || !$_GET['prev_building_id']) {
                    return [
                        'code' => 99,
                        'msg' => 'Previous asset location is not complete, please fix the data in menu asset list',
                    ];
                }
            
                try {
                    $insert = [
                        'asset_id' => $_GET['item_code'],
                        'mv_assigned_user' => $_GET['mv_assigned_user'],
                        'mv_location_id' => $_GET['mv_location_id'],
                        'mv_department_id' => $_GET['mv_department_id'],
                        'mv_area_id' => $_GET['mv_area_id'],
                        'mv_building_id' => $_GET['mv_building_id'],
                        'mv_priority' => $_GET['mv_priority'],
                        'remarks' => $_GET['mv_remarks'],
                        'created_by' => session('users')->user_id,
                        "prev_location_id" => $_GET['prev_location_id'],
                        "prev_department_id" => $_GET['prev_department_id'],
                        "prev_area_id" => $_GET['prev_area_id'],
                        "prev_building_id" => $_GET['prev_building_id'],
                        "prev_assigned_user" => $_GET['prev_assigned_user']
                    ];

                    $dept = session('users')->department_id;
                    $approver = DB::table('users')->where([
                        'role' => 3,
                        'department_id' => $dept,
                        'location_id' => $_GET['prev_location_id']
                    ])->get();

                    DB::table('asset_movement')->insert($insert);

                    foreach ($approver as $key => $value) {
                        $insert['mailto'] = $value->name;
                        Mail::to($value->email)->send(new Movement($insert));
                    }

                    return [
                        'code' => 0,
                        'msg' => 'Success',
                        'redirect' => route('asset-operation.asset-movement.index')
                    ];
                } catch (\Exception $e) {
                    return [
                        'code' => 99,
                        'msg' => $e->getMessage()
                    ];
                }
            }

            return view('asset-operation.asset-movement.add', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function detail(Request $req) {
        try {
            $movement_id = $req->id;
            $data = DB::table('v_asset_movement')->where('movement_id', $movement_id)->first();
            $param = [
                'title' => 'Asset Movement > Detail',
                'data' => $data
            ];

            return view('asset-operation.asset-movement.detail', $param);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors([
                'error' => $e->getMessage()
            ])->withInput();
        }
    }

    public function approve(Request $req) {
        try {
            $movementId = $req->id;
            $users = DB::table('users')->where('role', 7)->get();
            $assetMv = DB::table('v_asset_movement')->where('movement_id', $movementId)->first();
            $insert = [
                'created_by' => $assetMv->created_name,
                'asset_code' => $assetMv->asset_code,
                'asset_name' => $assetMv->asset_name
            ];

            foreach ($users as $key => $value) {
                $insert['mailto'] = $value->name;
                Mail::to($value->email)->send(new MovementAssetMgt($insert));
            }

            DB::table('asset_movement')->where('movement_id', $movementId)->update([
                'status' => 1,
                'approved_by' => session('users')->user_id
            ]);
            return redirect()->back()->withErrors([
                'success' => 'asset has been approved successfully'
            ])->withInput();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors([
                'error' => $e->getMessage()
            ])->withInput();
        }
    }

    public function reject(Request $req) {
        try {
            $movementId = $req->id;
            DB::table('asset_movement')->where('movement_id', $movementId)->update([
                'status' => 2,
                'reason_from_head' => $req->reason,
                'approved_by' => session('users')->user_id
            ]);
            return redirect()->back()->withErrors([
                'success' => 'asset has been rejected successfully'
            ])->withInput();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors([
                'error' => $e->getMessage()
            ])->withInput();
        }
    }

    public function approveValid(Request $req) {
        try {
            $movementId = $req->id;
            $data = DB::table('asset_movement')->where('movement_id', $movementId)->first();
            DB::beginTransaction();
            DB::table('asset_movement')->where('movement_id', $movementId)->update([
                'validate' => 1,
                'validated_by' => session('users')->user_id
            ]);
            DB::table('asset_list')->where('asset_id', $data->asset_id)->update([
                'location_id' => $data->mv_location_id,
                'department_id' => $data->mv_department_id,
                'area_id' => $data->mv_area_id,
                'floor' => null,
                'building_id' => $data->mv_building_id,
                'assigned_user' => $data->mv_assigned_user
            ]);
            DB::commit();
            return redirect()->back()->withErrors([
                'success' => 'asset has been validated successfully'
            ])->withInput();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors([
                'error' => $e->getMessage()
            ])->withInput();
        }
    }

    public function rejectValid(Request $req) {
        try {
            $movementId = $req->id;
            DB::table('asset_movement')->where('movement_id', $movementId)->update([
                'validate' => 2,
                'reason_from_assetmgt' => $req->reason,
                'validated_by' => session('users')->user_id
            ]);
            return redirect()->back()->withErrors([
                'success' => 'asset has been rejected successfully'
            ])->withInput();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors([
                'error' => $e->getMessage()
            ])->withInput();
        }
    }
}
