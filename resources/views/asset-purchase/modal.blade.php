                    @php
                        if (session('users')->role == 4 && $data->order_status == 8) {
                            $modalSize = 'modal-lg';
                            $customSize = 'max-width: 60%';
                            $title = 'Validation';
                        } else {
                            $modalSize = '';
                            $customSize = '';
                            $title = 'Approve Confirmation';
                        }
                    @endphp
                    <style>
                        #tblPayment td{
                            width: 25%;
                            padding-bottom: 15px;
                        }
                    </style>
                    <div class="modal fade text-left" id="approve" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable {{ $modalSize }}" role="document" style="{{ $customSize }}">
                            <div class="modal-content">
                                <form method="post" action="{{route('asset-purchase.approval','test')}}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel1">{{ $title }}</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" name="id" id="id" value="">
                                        <input type="hidden" name="act" id="act" value="">
                                        <input type="hidden" name="status" id="act" value="0">
                                @if ($data->order_status != 8)
                                        <p>Are you sure want approve this request ?</p>
                                        @if ($data->order_status != 9)
                                        <label for="comment">Comment*</label>
                                        <textarea name="comment" class="form-control" cols="30" required></textarea>
                                        @endif
                                        @if (session('users')->role == 4)
                                        <br>
                                        <label for="max_budget">Maxium Budget (IDR)*</label>
                                        <input type="text" class="form-control money" name="max_budget" required>
                                        <br>
                                        <label for="payment_method">Payment Method*</label>
                                        <select name="payment_method" class="form-control" required>
                                            <option></option>
                                            <option value="0">Full Payment*</option>
                                            <option value="1">Installment</option>
                                        </select>
                                        @endif
                                        @if (session('users')->role == 6 && $data->order_status != 8)
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="po_id">PO Number</label>
                                                <input type="text" class="form-control" value="PO-{{ str_pad($data->request_id, 5, '0', STR_PAD_LEFT) }}" readonly required><br>
                                                <label for="adv_pay">Need Advance Payment*</label>
                                                <select name="advance_payment" class="form-control" required>
                                                    <option value="0">No</option>
                                                    <option value="1">Yes</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="max_budget">Suplier Name*</label>
                                                <select name="supplier_id" class="form-control select2" required>
                                                    <option></option>
                                                    @foreach ($supplier as $item)
                                                    <option value="{{ $item->supplier_id }}">{{ $item->name }}</option>
                                                    @endforeach
                                                </select><br><br>
                                                <label for="max_budget">Amount (IDR)*</label>
                                                <input type="text" class="form-control money" name="amount" required>
                                            </div>
                                        </div>
                                        @endif
                                    @else 
                                        <p>Are you sure this item already <b>paid</b> ?</p>
                                        <table class="w-100" id="tblPayment">
                                            <tr>
                                                <td>
                                                    <label for="currency">Currency*</label>
                                                    <input type="text" class="form-control" value="IDR" readonly>
                                                </td>
                                                <td>
                                                    <label for="depreciation_method">Depreciation Method*</label>
                                                    <select name="depreciation_method" class="form-control">
                                                        <option value="1">Declining Balance</option>
                                                        <option value="2">Double Declining Balance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <label for="life_time">Life Time (month)*</label>
                                                    <input type="text" name="life_time" class="form-control only-number" placeholder="Life Time (Month)" required>
                                                </td>
                                                <td>
                                                    <label for="payment_method">Payment Method*</label>
                                                    <input type="text" class="form-control" value="{{ $data->payment_method==0?'Full Payment':'Installment' }}" readonly>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <label for="actual_cost">Total Actual Cost Amount*</label>
                                                    <input type="text" name="actual_cost" class="form-control money" placeholder="Total Actual Cost Amount" required>
                                                </td>
                                                <td colspan="2">
                                                    <label for="invoice">Invoice (pdf)*</label>
                                                    <input type="file" class="form-control" name="invoice" accept=".pdf" required>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label for="residual_value">Estimated Residual Value*</label>
                                                    <input type="text" class="form-control money" name="residual_value" required>
                                                </td>
                                                <td>
                                                    <label for="entity">Entity*</label>
                                                    <select name="entity_id" class="form-control">
                                                        @foreach ($entity as $item)
                                                            <option value="{{ $item->entity_id }}">{{ $item->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>
                                                    <label for="type">Asset Type*</label>
                                                    <select name="type" class="form-control">
                                                        <option value="1">Fixed Asset</option>
                                                        <option value="2">Non Fixed Asset</option>
                                                    </select>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    @endif
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-danger">Yes</button>
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade text-left" id="reject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <form method="post" action="{{route('asset-purchase.approval','test')}}">
                                {{csrf_field()}}
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel1">Reject Confirmation</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" name="id" id="id" value="">
                                        <input type="hidden" name="act" id="act" value="">
                                        <input type="hidden" name="status" id="act" value="1">
                                        <p>Are you sure want reject this request ?</p>
                                        <label for="comment">Comment * :</label>
                                        <textarea name="comment" class="form-control" cols="30" required></textarea>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-danger">Yes</button>
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <script src="{{asset('app-assets/js/scripts/mask/jquery.mask.js')}}"></script>
                    <script>
                        $(function () {
                            $('.money').mask('000,000,000,000,000,000', {reverse: true});
                            $('.only-number').mask('000');
                            $('#approve').on('show.bs.modal', function (event) {
                                var button = $(event.relatedTarget) 
                                var id = button.data('id')
                                var modal = $(this)
                                modal.find('.modal-body #id').val(id)
                            });
                            $('#reject').on('show.bs.modal', function (event) {
                                var button = $(event.relatedTarget) 
                                var id = button.data('id')
                                var modal = $(this)
                                modal.find('.modal-body #id').val(id)
                            });
                        });
                    </script>