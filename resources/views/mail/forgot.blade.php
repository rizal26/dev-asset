<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style>
        .bootstrap-btn {
            display: inline-block;
            font-weight: 400;
            color: #212529;
            text-align: center;
            border: 1px solid transparent;
            padding: .375rem .75rem;
            font-size: 1rem;
            line-height: 1.5;
            border-radius: .25rem;
            color: #fff;
            background-color: #007bff;
            text-decoration: none;
        }


        .bootstrap-btn:hover{
            background-color: #0069d9;

        }
    </style>
    <title>Authentication</title>
  </head>
  <body>
    <div style="margin: 30px; font-family: Arial, Helvetica, sans-serif;">
        <p><b>Hallo {{ $data[0]->name }},</b></p>
        <p>Forgot your password ?<br>
        We received a request to reset the password for your account.</p>

        <p>This following is your temporary login information :</p>
        <table>
            <tbody>
                <tr>
                    <td><b>Username/Email</b></td>
                    <td style="padding-left: 20px;padding-right: 20px;">:</td>
                    <td>{{ $data[0]->email }}</td>
                </tr>
                <tr>
                    <td><b>Password</b></td>
                    <td style="padding-left: 20px;padding-right: 20px;">:</td>
                    <td>{{ $data[1] }}</td>
                </tr>
            </tbody>
        </table>
        <br>
        <p>or you can directly login by click the button below</p>
        <a href="{{ route('login').'?email='.$data[0]->email.'&pass='.base64_encode($data[1]) }}" class="bootstrap-btn" target="_blank">Login</a>
        <br>
        <p>You can change password anytime you want in menu <b>Profile</b> once you have been logged in with the temporary password.</p>
        <p><b>Best Regards,<br>
        Admin Mitra Berlian Unggas</b><br>
        Jl. Mayang Padmi Kulon No. 31A<br>
        Kota Baru Parahyangan<br>
        Bandung, Jawa Barat<br>
        Indonesia 40553<br>
        +62 22 1000 189
        </p>
    </div>
  </body>
</html>

