@extends('templates.main')
@section('title', $title)
@section('content')
                @include('templates.message-validation')
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{$title}}</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form form-horizontal" method="post" action="{{ route('asset-operation.asset-maintenance.add') }}">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="asset_id">Asset Code</label>
                                                <select class="select2 w-100" name="asset_id" id="asset_id" >
                                                    <option></option>
                                                    @if (old('asset_id'))
                                                        @php
                                                            $asset = DB::table('asset_list')->where('asset_id', old('asset_id'))->first();
                                                        @endphp
                                                        <option value="{{ $asset->asset_id }}" selected>{{ $asset->item_code }}</option>
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="currency">Currency</label>
                                                <input type="text" name="currency" id="currency" class="form-control" placeholder="Currency" value="IDR" readonly/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="asset_name">Asset Name</label>
                                                <input type="text" id="asset_name" class="form-control" placeholder="Asset Name" readonly disabled />
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="total_cost">Total Cost</label>
                                                <input type="text" name="total_cost" id="total_cost" class="form-control money" placeholder="Total Cost" value="{{ old('total_cost') }}" />
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="dept_name">Department</label>
                                                <input type="text" id="dept_name" class="form-control" placeholder="Department" readonly disabled/>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="supplier_id">Vendor</label>
                                                <select class="select2 w-100" name="supplier_id" id="supplier_id" >
                                                    <option></option>
                                                    @foreach ($vendors as $item)
                                                        <option value="{{ $item->supplier_id }}" {{ old('supplier_id')==$item->supplier_id?'selected':'' }}>{{ $item->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="form-label" for="descr">Description</label>
                                                <textarea name="descr" id="descr" rows="5" class="form-control" placeholder="Description">{{ old('descr') }}</textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary mr-1 waves-effect waves-float waves-light">Submit</button>
                                                <a href="{{ route('asset-operation.asset-maintenance.index') }}" class="btn btn-outline-warning waves-effect">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                <script src="{{asset('app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
                <script src="{{asset('app-assets/js/scripts/mask/jquery.mask.js')}}"></script>

                <script>
                    $(function () {
                        $('select[name=asset_id]').trigger("change");
                        $('.money').mask('000,000,000,000,000,000', {reverse: true});
        
                        $('#supplier_id').select2({
                            placeholder: 'Select',
                            allowClear: true,
                        });

                        $('#asset_id').select2({
                            placeholder: 'Select',
                            allowClear: true,
                            ajax: {
                                url: "{{ route('select.asset-list') }}",
                                type: 'GET',
                                dataType: 'json',
                                data: function (params) {
                                    var qry = {
                                        search: params.term
                                    }
                                    return qry;
                                }
                            }
                        });

                        $('#asset_id').change(function (e) { 
                            e.preventDefault();
                            $.ajax({
                                type: "POST",
                                url: "{{ route('asset-operation.asset-list.ajax-get') }}",
                                data: { asset_id: $(this).val() },
                                success: function (response) {
                                    if (response.code == 99) {
                                        alert(response.msg);
                                        $('input').val('');
                                        return false;
                                    } else {
                                        $('#asset_name').val(response.data.asset_name);
                                        $('#dept_name').val(response.data.dept_name);

                                    }
                                }
                            });
                        });
                    });
                </script>
                                
@endsection