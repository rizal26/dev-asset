<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(!(Session::get('login'))){
        return redirect('login');
    }
    return redirect('home');
});

//Login
Route::match(['get','post'], '/login', 'App\Http\Controllers\AuthController@login')->name('login');
Route::get('/logout', 'App\Http\Controllers\AuthController@logout')->name('logout');
Route::any('/forgot', 'App\Http\Controllers\AuthController@forgot')->name('forgot');
Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home');
Route::get('/home/chart-category', 'App\Http\Controllers\HomeController@chartCategory')->name('home.chart.category');
Route::get('/home/chart-location', 'App\Http\Controllers\HomeController@chartLocation')->name('home.chart.location');
Route::get('/asset-detail/{code}', 'App\Http\Controllers\DetailController@detail')->name('asset-detail');

Route::group(['prefix' => 'asset-purchase'], function () {
    Route::any('/', 'App\Http\Controllers\AssetPurchase\PurchaseController@index')->name('asset-purchase.index');
    Route::any('/print', 'App\Http\Controllers\AssetPurchase\PurchaseController@print')->name('asset-purchase.print');
    
    Route::any('/step1', 'App\Http\Controllers\AssetPurchase\PurchaseController@step1')->name('asset-purchase.step1');
    Route::any('/detail/{id}', 'App\Http\Controllers\AssetPurchase\PurchaseController@detail')->name('asset-purchase.detail');
    Route::any('/approval', 'App\Http\Controllers\AssetPurchase\PurchaseController@approval')->name('asset-purchase.approval');
    Route::any('/validate', 'App\Http\Controllers\AssetPurchase\PurchaseController@validation')->name('asset-purchase.validate');
});

Route::group(['prefix' => 'todo'], function () {
    Route::any('/', 'App\Http\Controllers\TodoController@index')->name('todo');
    Route::any('/count', 'App\Http\Controllers\TodoController@todoCount')->name('todo.count');

});

Route::group(['prefix' => 'asset-operation'], function () {
    Route::get('/asset-list', 'App\Http\Controllers\AssetOperation\AssetListController@index')->name('asset-operation.asset-list.index');
    Route::post('/asset-list/ajax/get', 'App\Http\Controllers\AssetOperation\AssetListController@ajaxGet')->name('asset-operation.asset-list.ajax-get');
    Route::get('/asset-list/image/{id}', 'App\Http\Controllers\AssetOperation\AssetListController@image')->name('asset-operation.asset-list.image');
    Route::get('/asset-list/detail/{id}', 'App\Http\Controllers\AssetOperation\AssetListController@detail')->name('asset-operation.asset-list.detail');
    Route::post('/asset-list/detail/edit/{id}', 'App\Http\Controllers\AssetOperation\AssetListController@detailEdit')->name('asset-operation.asset-list.detail.edit');
    Route::post('/asset-list/detail/delete/document/{id}', 'App\Http\Controllers\AssetOperation\AssetListController@deleteDocument')->name('asset-operation.asset-list.detail.delete.document');
    Route::post('/print-label', 'App\Http\Controllers\AssetOperation\AssetListController@printLabel')->name('asset-operation.asset-list.print-label');

    Route::get('/asset-list/add', 'App\Http\Controllers\AssetOperation\AssetListController@add')->name('asset-operation.asset-list.add');

    Route::get('/asset-movement', 'App\Http\Controllers\AssetOperation\MovementController@index')->name('asset-operation.asset-movement.index');
    Route::get('/asset-movement/ajax', 'App\Http\Controllers\AssetOperation\MovementController@ajax')->name('asset-operation.asset-movement.ajax');
    Route::any('/asset-movement/add', 'App\Http\Controllers\AssetOperation\MovementController@add')->name('asset-operation.asset-movement.add');
    Route::any('/asset-movement/detail/{id}', 'App\Http\Controllers\AssetOperation\MovementController@detail')->name('asset-operation.asset-movement.detail');
    
    Route::post('/asset-movement/approve', 'App\Http\Controllers\AssetOperation\MovementController@approve')->name('asset-operation.asset-movement.approve');
    Route::post('/asset-movement/reject', 'App\Http\Controllers\AssetOperation\MovementController@reject')->name('asset-operation.asset-movement.reject');
    Route::post('/asset-movement/approve-valid', 'App\Http\Controllers\AssetOperation\MovementController@approveValid')->name('asset-operation.asset-movement.approve-valid');
    Route::post('/asset-movement/reject-valid', 'App\Http\Controllers\AssetOperation\MovementController@rejectValid')->name('asset-operation.asset-movement.reject-valid');
    
    Route::get('/asset-maintenance', 'App\Http\Controllers\AssetOperation\MaintenanceController@index')->name('asset-operation.asset-maintenance.index');
    Route::get('/asset-maintenance/ajax', 'App\Http\Controllers\AssetOperation\MaintenanceController@ajax')->name('asset-operation.asset-maintenance.ajax');
    Route::any('/asset-maintenance/add', 'App\Http\Controllers\AssetOperation\MaintenanceController@add')->name('asset-operation.asset-maintenance.add');
    Route::any('/asset-maintenance/detail/{id}', 'App\Http\Controllers\AssetOperation\MaintenanceController@detail')->name('asset-operation.asset-maintenance.detail');
    Route::post('/asset-maintenance/approve', 'App\Http\Controllers\AssetOperation\MaintenanceController@approve')->name('asset-operation.asset-maintenance.approve');
    Route::post('/asset-maintenance/reject', 'App\Http\Controllers\AssetOperation\MaintenanceController@reject')->name('asset-operation.asset-maintenance.reject');
    
});

Route::any('/select/department', 'App\Http\Controllers\AjaxController@department')->name('select.department');
Route::any('/select/building', 'App\Http\Controllers\AjaxController@building')->name('select.building');
Route::any('/select/area', 'App\Http\Controllers\AjaxController@area')->name('select.area');
Route::any('/select/floor', 'App\Http\Controllers\AjaxController@floor')->name('select.floor');
Route::any('/select/asset', 'App\Http\Controllers\AjaxController@asset')->name('select.asset');
Route::any('/select/asset-list', 'App\Http\Controllers\AjaxController@assetList')->name('select.asset-list');

Route::group(['prefix' => 'data-master'], function () {
    Route::group(['prefix' => 'asset'], function () {
        Route::get('/', 'App\Http\Controllers\DataMaster\AssetController@index')->name('data-master.asset.index');
        Route::any('/add', 'App\Http\Controllers\DataMaster\AssetController@add')->name('data-master.asset.add');
        Route::any('/edit/{id}', 'App\Http\Controllers\DataMaster\AssetController@edit')->name('data-master.asset.edit');
        Route::post('/delete/{id}', 'App\Http\Controllers\DataMaster\AssetController@delete')->name('data-master.asset.delete');
    });

    Route::group(['prefix' => 'category'], function () {
        Route::get('/', 'App\Http\Controllers\DataMaster\CategoryController@index')->name('data-master.category.index');
        Route::any('/add', 'App\Http\Controllers\DataMaster\CategoryController@add')->name('data-master.category.add');
        Route::any('/edit/{id}', 'App\Http\Controllers\DataMaster\CategoryController@edit')->name('data-master.category.edit');
        Route::post('/delete/{id}', 'App\Http\Controllers\DataMaster\CategoryController@delete')->name('data-master.category.delete');
    });
    
    Route::group(['prefix' => 'entity'], function () {
        Route::get('/', 'App\Http\Controllers\DataMaster\EntityController@index')->name('data-master.entity.index');
        Route::any('/add', 'App\Http\Controllers\DataMaster\EntityController@add')->name('data-master.entity.add');
        Route::any('/edit/{id}', 'App\Http\Controllers\DataMaster\EntityController@edit')->name('data-master.entity.edit');
        Route::post('/delete/{id}', 'App\Http\Controllers\DataMaster\EntityController@delete')->name('data-master.entity.delete');
    });

    Route::group(['prefix' => 'department'], function () {
        Route::get('/', 'App\Http\Controllers\DataMaster\DepartmentController@index')->name('data-master.department.index');
        Route::any('/add', 'App\Http\Controllers\DataMaster\DepartmentController@add')->name('data-master.department.add');
        Route::any('/edit/{id}', 'App\Http\Controllers\DataMaster\DepartmentController@edit')->name('data-master.department.edit');
        Route::post('/delete/{id}', 'App\Http\Controllers\DataMaster\DepartmentController@delete')->name('data-master.department.delete');
    });

    Route::group(['prefix' => 'district'], function () {
        Route::get('/', 'App\Http\Controllers\DataMaster\DistrictController@index')->name('data-master.district.index');
        Route::any('/add', 'App\Http\Controllers\DataMaster\DistrictController@add')->name('data-master.district.add');
        Route::any('/edit/{id}', 'App\Http\Controllers\DataMaster\DistrictController@edit')->name('data-master.district.edit');
        Route::post('/delete/{id}', 'App\Http\Controllers\DataMaster\DistrictController@delete')->name('data-master.district.delete');
    });

    Route::group(['prefix' => 'location'], function () {
        Route::get('/', 'App\Http\Controllers\DataMaster\LocationController@index')->name('data-master.location.index');
        Route::post('/select', 'App\Http\Controllers\DataMaster\LocationController@selectLocation')->name('data-master.location.select');
        Route::any('/add', 'App\Http\Controllers\DataMaster\LocationController@add')->name('data-master.location.add');
        Route::any('/edit/{id}', 'App\Http\Controllers\DataMaster\LocationController@edit')->name('data-master.location.edit');
        Route::post('/delete/{id}', 'App\Http\Controllers\DataMaster\LocationController@delete')->name('data-master.location.delete');
    });
    
    Route::group(['prefix' => 'area'], function () {
        Route::get('/', 'App\Http\Controllers\DataMaster\AreaController@index')->name('data-master.area.index');
        Route::post('/select', 'App\Http\Controllers\DataMaster\AreaController@selectArea')->name('data-master.area.select');
        Route::any('/add', 'App\Http\Controllers\DataMaster\AreaController@add')->name('data-master.area.add');
        Route::any('/edit/{id}', 'App\Http\Controllers\DataMaster\AreaController@edit')->name('data-master.area.edit');
        Route::post('/delete/{id}', 'App\Http\Controllers\DataMaster\AreaController@delete')->name('data-master.area.delete');
    });

    Route::group(['prefix' => 'supplier'], function () {
        Route::get('/', 'App\Http\Controllers\DataMaster\SupplierController@index')->name('data-master.supplier.index');
        Route::any('/add', 'App\Http\Controllers\DataMaster\SupplierController@add')->name('data-master.supplier.add');
        Route::any('/edit/{id}', 'App\Http\Controllers\DataMaster\SupplierController@edit')->name('data-master.supplier.edit');
        Route::post('/delete/{id}', 'App\Http\Controllers\DataMaster\SupplierController@delete')->name('data-master.supplier.delete');
    });
});

Route::group(['prefix' => 'users'], function () {
    Route::get('/', 'App\Http\Controllers\Users\UsersController@index')->name('users.index');
    Route::post('/ajax/get', 'App\Http\Controllers\Users\UsersController@ajaxGet')->name('users.ajax-get');
    Route::any('/add', 'App\Http\Controllers\Users\UsersController@add')->name('users.add');
    Route::any('/edit/{id}', 'App\Http\Controllers\Users\UsersController@edit')->name('users.edit');
    Route::post('/delete/{id}', 'App\Http\Controllers\Users\UsersController@delete')->name('users.delete');
});



