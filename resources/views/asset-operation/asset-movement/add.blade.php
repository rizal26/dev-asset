@extends('templates.main')
@section('title', $title)
@section('content')
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/plugins/forms/form-validation.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/plugins/forms/form-wizard.css')}}">

                @if($errors->any())
                    @foreach($errors->all() as $error)
                        @if($errors->has('success'))
                        <div class="alert alert-success">
                            <div class="alert-body">
                                <strong>{{ $error }}</strong>
                            </div>
                        </div>
                        @else
                        <div class="alert alert-danger">
                            <div class="alert-body">
                                <strong>{{ $error }}</strong>
                            </div>
                        </div>
                        @endif    
                    @endforeach
                @endif
                <div id="resp"></div>
                <!-- Horizontal Wizard -->
                <section class="horizontal-wizard">
                    <div class="bs-stepper horizontal-wizard-example">
                        <div class="bs-stepper-header">
                            <div class="step" data-target="#account-details">
                                <button type="button" class="step-trigger">
                                    <span class="bs-stepper-box">1</span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">Asset Movement Object</span>
                                        <span class="bs-stepper-subtitle">Choose Asset Detail</span>
                                    </span>
                                </button>
                            </div>
                            <div class="line">
                                <i data-feather="chevron-right" class="font-medium-2"></i>
                            </div>
                            <div class="step" data-target="#personal-info">
                                <button type="button" class="step-trigger">
                                    <span class="bs-stepper-box">2</span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">Movement Info</span>
                                        <span class="bs-stepper-subtitle">Destination Detail</span>
                                    </span>
                                </button>
                            </div>
                        </div>
                        <div class="bs-stepper-content">
                            <div id="account-details" class="content">
                                <form id="step1">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="asset_code">Asset Code</label>
                                            <select class="select2 w-100" name="item_code" id="item_code">
                                                <option></option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="asset_name">Asset Name</label>
                                            <input type="text" name="asset_name" id="asset_name" class="form-control" placeholder="Asset Name" readonly disabled />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="brand">Brand</label>
                                            <input type="text" name="brand" id="brand" class="form-control" placeholder="Brand" readonly disabled/>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="spec">Specification</label>
                                            <input type="text" name="spec" id="spec" class="form-control" placeholder="Specification" readonly disabled/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="sn">Serial Number</label>
                                            <input type="text" name="sn" id="sn" class="form-control" placeholder="Serial Number" readonly disabled/>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="location_name">Location</label>
                                            <input type="text" name="location_name" id="location_name" class="form-control" placeholder="Location" readonly disabled/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="building_name">Building</label>
                                            <input type="text" name="building_name" id="building_name" class="form-control" placeholder="Building" readonly disabled/>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="assigned_user">Assigned User</label>
                                            <input type="text" name="assigned_user" id="assigned_user" class="form-control" placeholder="Assigned User" readonly disabled/>
                                        </div>
                                    </div>
                                </form>
                                <div class="d-flex justify-content-between">
                                    <button class="btn btn-outline-secondary btn-prev" disabled>
                                        <i data-feather="arrow-left" class="align-middle mr-sm-25 mr-0"></i>
                                        <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                    </button>
                                    <button class="btn btn-primary btn-next">
                                        <span class="align-middle d-sm-inline-block d-none">Next</span>
                                        <i data-feather="arrow-right" class="align-middle ml-sm-25 ml-0"></i>
                                    </button>
                                </div>
                            </div>
                            <div id="personal-info" class="content">
                                <form id="step2">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="mv_assigned_user">Assigned User</label>
                                            <select class="select2 w-100" name="mv_assigned_user" id="mv_assigned_user">
                                                <option label=" "></option>
                                                @foreach ($users as $item)
                                                    <option value="{{ $item->name }}">{{ $item->name }}</option> 
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="mv_location_id">Location</label>
                                            <select class="select2 w-100" name="mv_location_id" id="mv_location_id">
                                                <option label=" "></option>
                                                @foreach ($loclist as $item)
                                                    <option value="{{ $item->location_id }}">{{ $item->name }}</option> 
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="mv_department">Department</label>
                                            <input type="text" class="form-control" name="mv_department" id="mv_department" placeholder="Department" readonly/>
                                            <input type="hidden" name="mv_department_id" id="mv_department_id" />
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="mv_building_id">Building</label>
                                            <select class="select2 w-100" name="mv_building_id" id="mv_building_id">
                                                <option label=" "></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <label class="form-label" for="mv_area_id">Room/Area</label>
                                                    <select class="select2 w-100" name="mv_area_id" id="mv_area_id">
                                                        <option label=" "></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <label class="form-label" for="mv_priority">Priority Level</label>
                                                    <select class="select2 w-100" name="mv_priority" id="mv_priority">
                                                        <option label=" "></option>
                                                        <option value="1">High</option>
                                                        <option value="2">Medium</option>
                                                        <option value="3">Low</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="mv_remarks">Remarks</label>
                                            <textarea name="mv_remarks" id="mv_remarks" cols="30" rows="4" class="form-control" placeholder="Remarks"></textarea>
                                        </div>
                                    </div>
                                    <input type="hidden" id="prev_location_id" name="prev_location_id">
                                    <input type="hidden" id="prev_department_id" name="prev_department_id">
                                    <input type="hidden" id="prev_area_id" name="prev_area_id">
                                    <input type="hidden" id="prev_building_id" name="prev_building_id">
                                    <input type="hidden" id="prev_assigned_user" name="prev_assigned_user">
                                    
                                </form>
                                <div class="d-flex justify-content-between">
                                    <button class="btn btn-primary btn-prev">
                                        <i data-feather="arrow-left" class="align-middle mr-sm-25 mr-0"></i>
                                        <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                    </button>
                                    <button class="btn btn-success btn-submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- /Horizontal Wizard -->
                <script src="{{asset('app-assets/vendors/js/forms/wizard/bs-stepper.min.js')}}"></script>
                <script src="{{asset('app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
                <script src="{{asset('app-assets/vendors/js/forms/validation/jquery.validate.min.js')}}"></script>
                <script src="{{asset('app-assets/js/scripts/forms/form-wizard.js')}}"></script>

                <script>
                    $(function () {
                        $('#item_code').change(function (e) { 
                            e.preventDefault();
                            $.ajax({
                                type: "POST",
                                url: "{{ route('asset-operation.asset-list.ajax-get') }}",
                                data: { asset_id: $(this).val() },
                                beforeSend: function () {
                                    $('input').val('');
                                },
                                success: function (response) {
                                    if (response.code == 99) {
                                        alert(response.msg);
                                        $('input').val('');
                                        return false;
                                    } else {
                                        $('#asset_name').val(response.data.asset_name);
                                        $('#brand').val(response.data.brand);
                                        $('#spec').val(response.data.spec);
                                        $('#sn').val(response.data.sn);
                                        $('#location_name').val(response.data.location_name);
                                        $('#building_name').val(response.data.building_name);
                                        $('#assigned_user').val(response.data.assigned_user);
                                        $('#prev_location_id').val(response.data.location_id);
                                        $('#prev_department_id').val(response.data.department_id);
                                        $('#prev_area_id').val(response.data.area_id);
                                        $('#prev_building_id').val(response.data.building_id);
                                        $('#prev_assigned_user').val(response.data.assigned_user);

                                    }
                                }
                            });
                        });

                        $('#item_code').select2({
                            placeholder: 'Select',
                            allowClear: true,
                            ajax: {
                                url: "{{ route('select.asset-list') }}",
                                type: 'GET',
                                dataType: 'json',
                                data: function (params) {
                                    var qry = {
                                        search: params.term
                                    }
                                    return qry;
                                }
                            }
                        });

                        $('#mv_assigned_user').change(function (e) { 
                            e.preventDefault();
                            $.ajax({
                                type: "POST",
                                url: "{{ route('users.ajax-get') }}",
                                data: { name: $(this).val() },
                                beforeSend: function () {
                                    $('#mv_department').val('');
                                    $('#mv_department_id').val('');
                                },
                                success: function (response) {
                                    if (response.code == 99) {
                                        alert(response.msg);
                                        $('#mv_department').val('');
                                        $('#mv_department_id').val('');
                                        return false;
                                    } else {
                                        $('#mv_department').val(response.data.department_name);
                                        $('#mv_department_id').val(response.data.department_id);
                                    }
                                }
                            });
                        });

                        $('#mv_location_id').change(function (e) { 
                            e.preventDefault();
                            $('#mv_building_id').html('');
                            $('#mv_area_id').html('');
                        });

                        $('#mv_building_id').change(function (e) { 
                            e.preventDefault();
                            $('#mv_area_id').html('');
                        });
                        
                        $('#mv_building_id').select2({
                            allowClear: true,
                            placeholder: "Select Value",
                            ajax: {
                                url: "{{ route('data-master.building.select') }}",
                                type: 'POST',
                                dataType: 'json',
                                data: function (params) {
                                    var qry = {
                                        search: params.term,
                                        location_id: $('#mv_location_id').val(),
                                    }
                                    return qry;
                                }
                            }
                        });

                        $('#mv_area_id').select2({
                            allowClear: true,
                            placeholder: "Select Value",
                            ajax: {
                                url: "{{ route('data-master.area.select') }}",
                                type: 'POST',
                                dataType: 'json',
                                data: function (params) {
                                    var qry = {
                                        search: params.term,
                                        building_id: $('#mv_building_id').val(),
                                    }
                                    return qry;
                                }
                            }
                        });

                        $('.btn-submit').click(function (e) { 
                            e.preventDefault();
                            var arrVal = $("form").serialize();
                            var isValid = $(this).parent().siblings('form').valid();
                            if (isValid) {
                                $.ajax({
                                    type: "get",
                                    url: "{{ route('asset-operation.asset-movement.add') }}"+"?"+arrVal,
                                    beforeSend: function () {
                                        $('.btn-submit').attr('disabled', '');
                                        $('#resp').html('');
                                    },
                                    success: function (response) {
                                        $('.btn-submit').removeAttr('disabled');
                                        if (response.code == 99) {
                                            $('#resp').html('<div class="alert alert-danger">'+
                                            '<div class="alert-body">'+
                                                '<strong>'+response.msg+'</strong>'+
                                            '</div>'+
                                            '</div>');
                                        } else {
                                            window.location = response.redirect+'?resp='+response.code;
                                        }
                                    }
                                });
                            }
                        });
                    });
                </script>
@endsection