<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AjaxController extends Controller
{
    public function __construct() {
        $this->middleware(function ($request, $next) {
            if (!session('login')) {
                return redirect()->route('login')->send();
                die();
            };
            return $next($request);
        });
    }
    
    public function department(Request $req) {
        $data = DB::table('department')
            ->when($req->has("entity_id"), function($q)use($req){
                return $q->where("entity_id", $req->entity_id);
            })
            ->where('name', 'like', '%'.$req->search.'%')
            ->where('deleted', 0)
            ->get();
        $output['results'] = array();
        for ($i=0; $i < count($data); $i++) { 
            $output['results'][] = [
                'id' => $data[$i]->department_id,
                'text' => $data[$i]->name
            ];
        }
        
        return json_encode($output);
    }

    public function building(Request $req) {
        $data = DB::table('building')
            ->when($req->has("location_id"), function($q)use($req){
                return $q->where("location_id", $req->location_id);
            })
            ->where('name', 'like', '%'.$req->search.'%')
            ->where('deleted', 0)
            ->get();
        $output['results'] = array();
        for ($i=0; $i < count($data); $i++) { 
            $output['results'][] = [
                'id' => $data[$i]->building_id,
                'text' => $data[$i]->name
            ];
        }
        
        return json_encode($output);
    }

    public function area(Request $req) {
        $data = DB::table('area')
            ->when($req->has("building_id"), function($q)use($req){
                return $q->where("building_id", $req->building_id);
            })
            ->where('name', 'like', '%'.$req->search.'%')
            ->where('deleted', 0)
            ->get();
        $output['results'] = array();
        for ($i=0; $i < count($data); $i++) { 
            $output['results'][] = [
                'id' => $data[$i]->area_id,
                'text' => $data[$i]->name
            ];
        }
        
        return json_encode($output);
    }

    public function assetList(Request $req) {
        $where = ['deleted' => 0];
        if (!in_array(session('users')->role, [1, 7])) {
            $where['location_id'] = session('users')->location_id;
            $where['department_id'] = session('users')->department_id;
        }
        $data = DB::table('asset_list')
            ->when($req->has("asset_id"), function($q)use($req){
                return $q->where("asset_id", $req->asset_id);
            })
            ->where('item_code', 'like', '%'.$req->search.'%')
            ->where($where)
            ->get();
        $output['results'] = array();
        for ($i=0; $i < count($data); $i++) { 
            $output['results'][] = [
                'id' => $data[$i]->asset_id,
                'text' => $data[$i]->item_code
            ];
        }
        
        return json_encode($output);
    }

    public function floor(Request $req) {
        try {
            $data = DB::table('area')
                ->where('area_id', $req->area_id)
                ->first()->floor;
            return ['data' => $data];
        } catch (\Exception $e) {
            return ['data' => $e->getMessage()];
        }
    }

    public function asset(Request $req) {
        try {
            $data = DB::table('m_asset AS a')
                ->leftJoin('category AS b', 'a.category_id', '=', 'b.category_id')
                ->where('a.m_asset_id', $req->m_asset_id)
                ->select('a.*', 'b.name AS category_name')
                ->first();
            return ['data' => $data->category_name, 'id' => $data->category_id];
        } catch (\Exception $e) {
            return ['data' => $e->getMessage(), 'id' => null];
        }
    }
}
