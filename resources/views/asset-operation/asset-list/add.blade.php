@extends('templates.main')
@section('title', $title)
@section('content')
@php
    $generalLink = '';
    $generalAriaExpand = 'false';

    $locationLink = '';
    $locationAriaExpand = 'false';

    $infoLink = '';
    $infoAriaExpand = 'false';

    $otherLink = '';
    $otherAriaExpand = 'false';

    if (Request::has('tab')){
        if (Request::get('tab')=='general') {
            $generalLink = 'active';
            $generalAriaExpand = 'true';
        } elseif (Request::get('tab')=='location') {
            $locationLink = 'active';
            $locationAriaExpand = 'true';
        } elseif (Request::get('tab')=='info') {
            $infoLink = 'active';
            $infoAriaExpand = 'true';
        } elseif (Request::get('tab')=='other') {
            $otherLink = 'active';
            $otherAriaExpand = 'true';
        } 
    } else {
        $generalLink = 'active';
        $generalAriaExpand = 'true';
    }

    if(!in_array(session('users')->role, [1, 7])){
        $disabled = 'disabled';
    } else {
        $disabled = '';
    }

@endphp

            @if($errors->any())
                @foreach($errors->all() as $error)
                    @if($errors->has('success'))
                    <div class="alert alert-success">
                        <div class="alert-body">
                            <strong>{{ $error }}</strong>
                        </div>
                    </div>
                    @else
                    <div class="alert alert-danger">
                        <div class="alert-body">
                            <strong>{{ $error }}</strong>
                        </div>
                    </div>
                    @endif    
                @endforeach
            @endif
            <div class="row">
                <div class="col-12">
                    <h4 class="card-title">{{ $title }}</h4>
                </div>
            </div>

            <div class="content-body">
                <!-- account setting page -->
                <section id="page-account-settings">
                    <div class="row">
                        @include('asset-operation.asset-list.tab-add.leftmenu')
                        <!-- right content section -->
                        <div class="col-md-9">
                            <div class="card">
                                <div class="card-body">
                                    <div class="tab-content">
                                        <!-- general tab -->
                                        @include('asset-operation.asset-list.tab-add.general')
                                        <!--/ general tab -->
                                        
                                        <!-- Location & User -->
                                        @include('asset-operation.asset-list.tab-add.location')
                                        <!--/ change password -->
                                        
                                        <!-- information -->
                                        @include('asset-operation.asset-list.tab-add.info')
                                        <!--/ information -->
                                        
                                        <!-- other information -->
                                        @include('asset-operation.asset-list.tab-add.other')
                                        <!-- other information -->
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ right content section -->
                    </div>
                </section>
                <!-- / account setting page -->
            </div>
            
            <script src="{{asset('app-assets/js/scripts/pages/page-account-settings.js')}}"></script>
            @include('asset-operation.asset-list.tab-add.js')
@endsection