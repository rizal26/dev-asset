@extends('templates.main')
@section('title', $title)
@section('content')
    
                @if($errors->any())
                    @foreach($errors->all() as $error)
                        @if($errors->has('success'))
                        <div class="alert alert-success">
                            <div class="alert-body">
                                <strong>{{ $error }}</strong>
                            </div>
                        </div>
                        @else
                        <div class="alert alert-danger">
                            <div class="alert-body">
                                <strong>{{ $error }}</strong>
                            </div>
                        </div>
                        @endif    
                    @endforeach
                @endif
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="float-right">
                                        <a href="{{ route('asset-operation.asset-movement.add') }}" type="button" class="btn btn-outline-primary waves-effect">Add New</a>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-datatable">
                                        <div class="table-responsive mb-2">
                                            <table id="datatable" class="table table-sm table-bordered table-striped w-100 nowrap">
                                                <thead>
                                                        <th>ID</th>
                                                        <th>Asset Name</th>
                                                        <th>Asset Code</th>
                                                        <th>Date</th>
                                                        {{-- <th>Previous<br>Location</th>
                                                        <th>Destination<br>Location</th>
                                                        <th>Previous<br>Dept.</th>
                                                        <th>Destination<br>Dept.</th> --}}
                                                        <th>Officer Head<br>Status</th>
                                                        <th>Asset Mgt.<br>Status</th>
                                                        <th>Action</th>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade text-left" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                {{-- <form method="post" action="{{route('asset-operation.asset-movement.delete','test')}}">
                                {{csrf_field()}} --}}
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel1">Delete Confirmation</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" name="id" id="id" value="">
                                        <input type="hidden" name="act" id="act" value="">
                                        <p>Are you sure want to delete ?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-danger">Yes</button>
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                                    </div>
                                {{-- </form> --}}
                            </div>
                        </div>
                    </div>

                    <script src="{{asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
                    <script src="{{asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>

                    <script>
                        $(function () {
                            $('#datatable').DataTable({
                                scrollX: true,
                                responsive: true,
                                processing: true,
                                serverSide: true,
                                drawCallback: function( settings ) {
                                    feather.replace();
                                },
                                ajax: {
                                    url: "{{ route('asset-operation.asset-movement.ajax') }}",
                                },
                                columns: [
                                    { data: 'movement_code' },
                                    { data: 'asset_name' },
                                    { data: 'asset_code' },
                                    { data: 'created_date' },
                                    // { data: 'prev_location' },
                                    // { data: 'dest_location' },
                                    // { data: 'prev_department' },
                                    // { data: 'dest_department' },
                                    { data: 'status' },
                                    { data: 'validate' },
                                    { data: 'action' }
                                ],
                                order: [[0, 'desc']],
                            });

                            $('#delete').on('show.bs.modal', function (event) {
                                var button = $(event.relatedTarget) 
                                var id = button.data('id')
                                var modal = $(this)
                                modal.find('.modal-body #id').val(id)
                            });
                        });
                    </script>

@endsection