<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use DB;

class Movement extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $insert;
    public function __construct($insert)
    {
        $this->insert = $insert;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $param = $this->insert;
        $user = DB::table('users')->where('user_id', $param['created_by'])->first();
        $asset = DB::table('v_asset_list')->where('asset_id', $param['asset_id'])->first();
        $data = [
            'created_by' => !is_null($user)?$user->name:'No Name',
            'asset_code' => !is_null($asset)?$asset->asset_code:'undifined',
            'asset_name' => !is_null($asset)?$asset->asset_name:'undifined',
            'mailto' => $param['mailto']
        ];

        return $this->from(env('MAIL_FROM_ADDRESS'))
            ->subject('Dev Asset Portal - New Asset Movement Request')
            ->view('mail.movement')
            ->with($data);
    }
}
