<?php

namespace App\Http\Controllers\DataMaster;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;

class AreaController extends Controller
{
    public function __construct() {
        $this->middleware(function ($request, $next) {
            if (!session('login')) {
                return redirect()->route('login')->withErrors(['error' => 'Please sign in to continue']);
                die();
            };
            return $next($request);
        });
    }

    public function index(Request $req) {
        try {
            $data = DB::table('area as a')
                ->join('building as b', 'b.building_id', '=', 'a.building_id')
                ->join('location as c', 'c.location_id', '=', 'b.location_id')
                ->select('a.*', 'b.name AS building_name', 'c.name AS location_name')
                ->where('a.deleted', 0)->get();

            $param = [
                'title' => 'Data Master > Area',
                'data' => $data
            ];
            return view('data-master.area.index', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function add(Request $req) {
        try {
            $param = [
                'title' => 'Data Master > Area > Add',
                'location' => DB::table('location')->where('deleted', 0)->get(),
                'building' => DB::table('building')->where('deleted', 0)->get()
            ];

            if ($req->isMethod('post')) {
                $input = $req->all();
                unset($input['_token']);
                $building_id = $input['building_id'];
                $name = $input['name'];
                $floor = $input['floor'];
                $messages = [
                    'building_id.unique' => 'Given building, area name and floor has already been taken',
                    'building_id.required' => 'building field is required',
                    'name.required' => 'Name field is required',
                    'floor.required' => 'Floor field is required',
                ];
                $rules = [
                    'building_id' => [
                        'required',
                        Rule::unique('area')->where(function ($query) use($building_id,$name,$floor) {
                            return $query->where([
                                'building_id' => $building_id,
                                'name' => $name,
                                'floor' => $floor
                            ]);
                        })
                    ],
                    'name' => 'required|string|min:1|max:100',
                    'floor' => 'required|string|min:1|max:100'
                ];
                $validator = Validator::make($input, $rules, $messages);
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput();
                }
                DB::table('area')->insert($input);
                $success = ['success' => 'Data has been saved successfuly'];
                return redirect()->route('data-master.area.index')->withErrors($success);
            }


            return view('data-master.area.add', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function edit(Request $req) {
        try {
            $data = DB::table('area as a')
                ->join('building as b', 'b.building_id', '=', 'a.building_id')
                ->join('location as c', 'c.location_id', '=', 'b.location_id')
                ->select('a.*', 'b.name AS building_name', 'c.location_id')
                ->where(['a.deleted' => 0, 'area_id' => $req->id])->first();
            $location = DB::table('location')->where('deleted', 0)->get();
            $param = [
                'title' => 'Data Master > Area > Edit',
                'data' => $data,
                'location' => $location
            ];
            
            if ($req->isMethod('post')) {
                $input = $req->all();
                unset($input['_token']);
                $building_id = $input['building_id'];
                $name = $input['name'];
                $floor = $input['floor'];
                $messages = [
                    'building_id.unique' => 'Given building, area name and floor has already been taken',
                    'building_id.required' => 'Building field is required',
                    'name.required' => 'Name field is required',
                    'floor.required' => 'Floor field is required',
                ];

                $rules = [
                    'building_id' => [
                        'required',
                        Rule::unique('area')->where(function ($query) use($building_id,$name,$floor) {
                            return $query->where([
                                'building_id' => $building_id,
                                'name' => $name,
                                'floor' => $floor,
                            ]);
                        })->ignore($req->id, 'area_id')
                    ],
                    'name' => 'required|string|min:1|max:100',
                    'floor' => 'required|string|min:1|max:100'
                ];
                $validator = Validator::make($input, $rules, $messages);
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput();
                }
                DB::table('area')->where('area_id', $req->id)->update($input);
                $success = ['success' => 'Data has been updated successfuly'];
                return redirect()->route('data-master.area.index')->withErrors($success);
            }

            return view('data-master.area.edit', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function delete(Request $req) {
        try {
            DB::table('area')->where('area_id', $req->id)->update(['deleted' => 1]);
            $success = ['success' => 'Data has been deleted successfuly'];
            return redirect()->route('data-master.area.index')->withErrors($success);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function selectArea(Request $request) {
        $data = DB::table('area')
            ->where('name', 'like', '%'.$request->search.'%')
            ->where('building_id', $request->building_id)
            ->get();

        $output['results'] = array();
        for ($i=0; $i < count($data); $i++) { 
            $output['results'][] = [
                'id' => $data[$i]->area_id,
                'text' => $data[$i]->name
            ];
        }
        
        return \json_encode($output);
    }
}
