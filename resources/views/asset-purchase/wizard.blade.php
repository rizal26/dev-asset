<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">

<div class="wizard-progress">
  <div class="step {{ $progress['step1'] }}">
    Submitted
    <div class="node"></div>
  </div>
  <div class="step {{ $progress['step2'] }}">
    Dept. Head<br>Approval
    <div class="node"></div>
  </div>
  <div class="step {{ $progress['step3'] }}">
    Asset Mgt.<br>Approval
    <div class="node"></div>
  </div>
  <div class="step {{ $progress['step4'] }}">
    Finance<br>Approval
    <div class="node"></div>
  </div>
  <div class="step {{ $progress['step5'] }}">
    Finance Director<br>Approval (if needed)
    <div class="node"></div>
  </div>
  <div class="step {{ $progress['step6'] }}">
    purchasing<br>Approval
    <div class="node"></div>
  </div>
  <div class="step {{ $progress['step7'] }}">
    Vendor<br>Delivery
    <div class="node"></div>
  </div>
  <div class="step {{ $progress['step8'] }}">
    Delivered<br>to User
    <div class="node"></div>
  </div>
  <div class="step {{ $progress['step9'] }}">
    Paid to<br>Vendor
    <div class="node"></div>
  </div>
  <div class="step {{ $progress['step10'] }}">
    Asset<br>Recognize
    <div class="node"></div>
  </div>
</div>