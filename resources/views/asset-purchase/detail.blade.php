@extends('templates.main')
@section('title', $title)
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.css">

            @include('asset-purchase.wizard')
                @if($errors->any())
                    @foreach($errors->all() as $error)
                        @if($errors->has('success'))
                        <div class="alert alert-success">
                            <div class="alert-body">
                                <strong>{{ $error }}</strong>
                            </div>
                        </div>
                        @else
                        <div class="alert alert-danger">
                            <div class="alert-body">
                                <strong>{{ $error }}</strong>
                            </div>
                        </div>
                        @endif    
                    @endforeach
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">{{ $title }}</h4><br>
                                <div class="float-right">
                                    @if ($data->status == 0 && 
                                    (session('users')->role == 3 && $data->order_status == 1) || 
                                    (session('users')->role == 7 && $data->order_status == 2) ||
                                    (session('users')->role == 4 && $data->order_status == 3) ||
                                    (session('users')->role == 5 && $data->order_status == 4) ||
                                    (session('users')->role == 6 && $data->order_status == 5) ||
                                    (session('users')->role == 2 && $data->order_status == 6) ||
                                    (session('users')->role == 4 && $data->order_status == 8) ||
                                    (session('users')->role == 7 && $data->order_status == 9) 
                                    )
                                        <button class="btn btn-success waves-effect waves-float waves-light" data-id="{{ $data->request_id }}" data-toggle="modal" data-target="#approve">
                                            Approve
                                        </button>
                                        {{-- @if (!in_array(session('users')->role, [2,6,7]) && !in_array($data->order_status, [8,9,10]) ) --}}
                                        <button class="btn btn-danger waves-effect waves-float waves-light" data-id="{{ $data->request_id }}" data-toggle="modal" data-target="#reject">Reject</button>
                                        {{-- @endif --}}
                                    @endif
                                    <a href="{{ route('asset-purchase.index') }}" class="btn btn-outline-warning waves-effect">Cancel</a>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <table class="table table-striped">
                                            <tr>
                                                <td><b>Request Number</b></td>
                                                <td>:</td>
                                                <td>{{ $data->request_id }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Brand</b></td>
                                                <td>:</td>
                                                <td>{{ $data->brand }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Category</b></td>
                                                <td>:</td>
                                                <td>{{ $data->category_name }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Expected Date</b></td>
                                                <td>:</td>
                                                <td>{{ date('d-M-Y', strtotime($data->expected_date)) }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Remarks</b></td>
                                                <td>:</td>
                                                <td>{{ $data->remarks }}</td>
                                            </tr>
                                            
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <table class="table table-striped">
                                            <tr>
                                                <td><b>Item Name</b></td>
                                                <td>:</td>
                                                <td>{{ $data->asset_name }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Specification</b></td>
                                                <td>:</td>
                                                <td>{{ $data->spec }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Quantity</b></td>
                                                <td>:</td>
                                                <td>{{ $data->qty }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>UOM</b></td>
                                                <td>:</td>
                                                <td>{{ $data->uom_name }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Priority Level</b></td>
                                                <td>:</td>
                                                <td>
                                                    @php
                                                        $priority = 'Undifined';
                                                        if ($data->priority == 1) {
                                                            $priority = '<div class="badge badge-pill badge-glow badge-danger">High</div>';
                                                        } elseif ($data->priority == 2) {
                                                            $priority = '<div class="badge badge-pill badge-glow badge-warning">Medium</div>';
                                                        } else {
                                                            $priority = '<div class="badge badge-pill badge-glow badge-success">Low</div>';
                                                        }
                                                    @endphp
                                                    <?=$priority?>
                                                </td>
                                            </tr>
                                            
                                        </table>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-md-6">
                                        <table class="table table-striped">
                                            <tr>
                                                <td style="width: 45%"><b>User Dept. Head Comment</b></td>
                                                <td>:</td>
                                                <td>{{ $data->cmt_dept_head }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Asset Mgt. Comment</b></td>
                                                <td>:</td>
                                                <td>{{ $data->cmt_asset_mgt }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Finance Comment</b></td>
                                                <td>:</td>
                                                <td>{{ $data->cmt_finance }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Finance Dir. Comment</b></td>
                                                <td>:</td>
                                                <td>{{ $data->cmt_finance_director }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Purchasing Comment</b></td>
                                                <td>:</td>
                                                <td>{{ $data->cmt_purchasing }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Asset User Comment</b></td>
                                                <td>:</td>
                                                <td>{{ $data->cmt_user_asset }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                         <table class="table table-striped">
                                            <tr>
                                                <td style="width: 35%"><b>Maximal budget</b></td>
                                                <td>:</td>
                                                <td>{{ $data->max_budget?'IDR '.number_format($data->max_budget):'' }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Supplier</b></td>
                                                <td>:</td>
                                                <td>{{ $data->supplier_name }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Advance Payment</b></td>
                                                <td>:</td>
                                                <td>{{ $data->advance_payment==0?'No':'Yes' }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Amount</b></td>
                                                <td>:</td>
                                                <td>{{ $data->amount?'IDR '.number_format($data->amount):'' }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Total Actual Cost</b></td>
                                                <td>:</td>
                                                <td>{{ $data->actual_cost?'IDR '.number_format($data->actual_cost):'' }}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Invoice</b></td>
                                                <td>:</td>
                                                <td><?= $data->invoice?'<a href="'.asset('documents/'.$data->invoice).'" target="_blank">Download</a>':'' ?></td>
                                            </tr>
                                         </table>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                @include('asset-purchase.modal')
                <script src="{{asset('app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
                <script>
                    $(function () {
                        $('.select2').select2({
                            placeholder: 'Select',
                            allowClear: true
                        });
                    });
                </script>
@endsection