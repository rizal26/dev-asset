                                        <div class="tab-pane {{ $otherLink=='active'?'active':'fade' }}" id="account-vertical-other" role="tabpanel" aria-labelledby="account-pill-other" aria-expanded="{{ $otherAriaExpand }}">
                                            <!-- form -->
                                                @php
                                                    if($data->life_time && $data->residual_value && $data->purchase_cost) {
                                                        $currentValue = ( $data->purchase_cost - $data->residual_value ) / $data->life_time;
                                                        $currentAge = date('Y') - date('Y', strtotime($data->created_date));
                                                    } else {
                                                        $currentValue = 0;
                                                        $currentAge = '';
                                                    }
                                                @endphp
                                                <div class="row">
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Current Book Value</label>
                                                            <input type="text" class="form-control" value="{{ number_format($currentValue) }}" readonly/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Current Age</label>
                                                            <input type="text" class="form-control" value="{{ $currentAge }}" readonly/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Upload Document</label>
                                                            <input type="file" name="document" class="form-control" accept=".pdf, .xls, .xlsx, .doc, .docx" {{ $disabled }}>
                                                        </div>
                                                        @if (!is_null($data->document))
                                                            <a href="{{ asset('documents/'.$data->document) }}">Download Document</a><br>
                                                            @if ($disabled == '')
                                                            
                                                            @endif
                                                        @endif
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Remarks</label>
                                                            <textarea name="remarks" class="form-control" {{ $disabled }}>{{ $data->remarks }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            <!--/ form -->
                                        </div>
                                        

                                        