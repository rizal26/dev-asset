@extends('templates.main')
@section('title', $title)
@section('content')
    
                @if($errors->any())
                    @foreach($errors->all() as $error)
                        @if($errors->has('success'))
                        <div class="alert alert-success">
                            <div class="alert-body">
                                <strong>{{ $error }}</strong>
                            </div>
                        </div>
                        @else
                        <div class="alert alert-danger">
                            <div class="alert-body">
                                <strong>{{ $error }}</strong>
                            </div>
                        </div>
                        @endif    
                    @endforeach
                @endif
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{ $title }}</h4><br>
                                    <div class="float-right">
                                        @if (in_array(session('users')->role, [1,3,7]))
                                            @if ($data->status == 0 && session('users')->role != 7)
                                                <button class="btn btn-success waves-effect waves-float waves-light" data-id="{{ $data->movement_id }}" data-toggle="modal" data-target="#approve">Approve</button>
                                                <button class="btn btn-danger waves-effect waves-float waves-light" data-id="{{ $data->movement_id }}" data-toggle="modal" data-target="#reject">Reject</button>
                                            @endif
                                            @if ($data->validate == 0 && $data->status == 1 && session('users')->role != 3)
                                                <button class="btn btn-success waves-effect waves-float waves-light" data-id="{{ $data->movement_id }}" data-toggle="modal" data-target="#approveValid">Validate</button>
                                                <button class="btn btn-danger waves-effect waves-float waves-light" data-id="{{ $data->movement_id }}" data-toggle="modal" data-target="#rejectValid">Reject</button>
                                            @endif
                                        @endif
                                        <a href="{{ route('asset-operation.asset-movement.index') }}" class="btn btn-outline-warning waves-effect">Cancel</a>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <table class="table table-striped">
                                                <tr>
                                                    <td><b>Created By</b></td>
                                                    <td>:</td>
                                                    <td>{{ $data->created_name }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Created Date</b></td>
                                                    <td>:</td>
                                                    <td>{{ date('d-M-Y', strtotime($data->created_date)) }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Current Status<br>(Officer Head)</b></td>
                                                    <td>:</td>
                                                    <td>
                                                        @php
                                                            $status = 'Undifined';
                                                            if ($data->status == 0) {
                                                                $status = '<div class="badge badge-pill badge-glow badge-warning">Waiting for approval</div>';
                                                            } elseif ($data->status == 1) {
                                                                $status = '<div class="badge badge-pill badge-glow badge-success">Approved</div>';
                                                            } else {
                                                                $status = '<div class="badge badge-pill badge-glow badge-danger">Rejected</div>';
                                                            }
                                                        @endphp
                                                        <?=$status?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>Approval By</b></td>
                                                    <td>:</td>
                                                    <td>{{ !is_null($data->approved_name)?$data->approved_name:'-' }}</td>
                                                </tr>
                                                @if ($data->status == 2)
                                                    <tr>
                                                        <td><b>Reason From<br>Officer Head</b></td>
                                                        <td>:</td>
                                                        <td>{{ $data->reason_from_head }}</td>
                                                    </tr>
                                                @endif
                                            </table>
                                        </div>
                                        <div class="col-md-6">
                                            <table class="table table-striped">
                                                <tr>
                                                    <td><b>Current Validate<br>(Asset Management)</b></td>
                                                    <td>:</td>
                                                    <td>
                                                        @php
                                                            $validate = 'Undifined';
                                                            if ($data->validate == 0) {
                                                                $validate = '<div class="badge badge-pill badge-glow badge-warning">Waiting for Validation</div>';
                                                            } elseif ($data->validate == 1) {
                                                                $validate = '<div class="badge badge-pill badge-glow badge-success">Approved</div>';
                                                            } else {
                                                                $validate = '<div class="badge badge-pill badge-glow badge-danger">Rejected</div>';
                                                            }
                                                        @endphp
                                                        <?=$validate?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>Validation By</b></td>
                                                    <td>:</td>
                                                    <td>{{ !is_null($data->validated_name)?$data->validated_name:'-' }}</td>
                                                </tr>
                                                @if ($data->validate == 2)
                                                    <tr>
                                                        <td><b>Reason From<br>Asset Management</b></td>
                                                        <td>:</td>
                                                        <td>{{ $data->reason_from_assetmgt }}</td>
                                                    </tr>
                                                @endif
                                                <tr>
                                                    <td><b>Last Updated</b></td>
                                                    <td>:</td>
                                                    <td>{{ !is_null($data->updated_date)?date('d-M-Y', strtotime($data->updated_date)):'-' }}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-striped table-borderd">
                                                <tr>
                                                    <td colspan="3" class="text-center"><b>Previous Detail</b></td>
                                                </tr>
                                                <tr>
                                                    <td><b>Asset Code</b></td>
                                                    <td>:</td>
                                                    <td><a href="{{ route('asset-operation.asset-list.detail', $data->asset_id) }}" target="_blank">{{ $data->asset_code }}</a></td>
                                                </tr>
                                                <tr>
                                                    <td><b>Asset Name</b></td>
                                                    <td>:</td>
                                                    <td>{{ $data->asset_name }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Brand</b></td>
                                                    <td>:</td>
                                                    <td>{{ $data->brand }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Specification</b></td>
                                                    <td>:</td>
                                                    <td>{{ $data->spec }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Serial Number</b></td>
                                                    <td>:</td>
                                                    <td>{{ $data->sn }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Location</b></td>
                                                    <td>:</td>
                                                    <td>{{ $data->prev_location }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Building</b></td>
                                                    <td>:</td>
                                                    <td>{{ $data->prev_building }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Assigned User</b></td>
                                                    <td>:</td>
                                                    <td>{{ $data->prev_assigned_user }}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-striped table-borderd">
                                                <tr>
                                                    <td colspan="3" class="text-center"><b>Destination Detail</b></td>
                                                </tr>
                                                <tr>
                                                    <td><b>Assigned User</b></td>
                                                    <td>:</td>
                                                    <td>{{ $data->mv_assigned_user }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Location</b></td>
                                                    <td>:</td>
                                                    <td>{{ $data->dest_location }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Department</b></td>
                                                    <td>:</td>
                                                    <td>{{ $data->dest_department }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Building</b></td>
                                                    <td>:</td>
                                                    <td>{{ $data->dest_building }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Room/Area</b></td>
                                                    <td>:</td>
                                                    <td>{{ $data->dest_area }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Priority Level</b></td>
                                                    <td>:</td>
                                                    <td>
                                                        @if ($data->mv_priority == 1)
                                                            High
                                                        @elseif ($data->mv_priority == 2)
                                                            Medium
                                                        @else 
                                                            Low
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>Remarks</b></td>
                                                    <td>:</td>
                                                    <td>{{ $data->remarks }}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade text-left" id="approve" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <form method="post" action="{{route('asset-operation.asset-movement.approve','test')}}">
                                {{csrf_field()}}
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel1">Approve Confirmation</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" name="id" id="id" value="">
                                        <input type="hidden" name="act" id="act" value="">
                                        <p>Are you sure want approve this movement asset ?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-danger">Yes</button>
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade text-left" id="approveValid" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <form method="post" action="{{route('asset-operation.asset-movement.approve-valid','test')}}">
                                {{csrf_field()}}
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel1">Validation Confirmation</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" name="id" id="id" value="">
                                        <input type="hidden" name="act" id="act" value="">
                                        <p>Are you sure want validate this movement asset ?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-danger">Yes</button>
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade text-left" id="reject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <form method="post" action="{{route('asset-operation.asset-movement.reject','test')}}">
                                {{csrf_field()}}
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel1">Reject Confirmation</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" name="id" id="id" value="">
                                        <input type="hidden" name="act" id="act" value="">
                                        <p>Are you sure want reject this movement asset ?</p>
                                        <label for="">Reason message :</label>
                                        <textarea name="reason" cols="30" placeholder="Reason of rejection" class="form-control" required></textarea>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-danger">Yes</button>
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade text-left" id="rejectValid" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <form method="post" action="{{route('asset-operation.asset-movement.reject-valid','test')}}">
                                {{csrf_field()}}
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel1">Reject Confirmation</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" name="id" id="id" value="">
                                        <input type="hidden" name="act" id="act" value="">
                                        <p>Are you sure want reject this movement asset ?</p>
                                        <label for="">Reason message :</label>
                                        <textarea name="reason" cols="30" placeholder="Reason of rejection" class="form-control" required></textarea>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-danger">Yes</button>
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <script>
                        $(function () {
                            $('#approve').on('show.bs.modal', function (event) {
                                var button = $(event.relatedTarget) 
                                var id = button.data('id')
                                var modal = $(this)
                                modal.find('.modal-body #id').val(id)
                            });

                            $('#reject').on('show.bs.modal', function (event) {
                                var button = $(event.relatedTarget) 
                                var id = button.data('id')
                                var modal = $(this)
                                modal.find('.modal-body #id').val(id)
                            });

                            $('#approveValid').on('show.bs.modal', function (event) {
                                var button = $(event.relatedTarget) 
                                var id = button.data('id')
                                var modal = $(this)
                                modal.find('.modal-body #id').val(id)
                            });

                            $('#rejectValid').on('show.bs.modal', function (event) {
                                var button = $(event.relatedTarget) 
                                var id = button.data('id')
                                var modal = $(this)
                                modal.find('.modal-body #id').val(id)
                            });
                        });
                    </script>

@endsection