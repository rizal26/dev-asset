                                        <div class="tab-pane {{ $infoLink=='active'?'active':'fade' }}" id="account-vertical-info" role="tabpanel" aria-labelledby="account-pill-info" aria-expanded="{{ $infoAriaExpand }}">
                                            <!-- form -->
                                                <div class="row">
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Purchase Cost</label>
                                                            <input type="text" class="form-control money" name="purchase_cost" value="{{ $data->purchase_cost }}" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Currency</label>
                                                            <input type="text" class="form-control" name="currency" value="{{ $data->currency }}" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Purchase Date</label>
                                                            <input type="text" id="fp-default" name="purchase_date" value="{{ date('d-M-Y', strtotime($data->purchase_date)) }}" class="form-control" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">PO Number</label>
                                                            <input type="text" class="form-control" name="po_number" value="{{ $data->po_number }}" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Supplier</label>
                                                            <input type="text" class="form-control" name="po_number" value="{{ $data->supplier_name }}" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Residual Value</label>
                                                            <input type="text" class="form-control money" name="residual_value" value="{{ $data->residual_value }}" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Depreciation Method</label>
                                                            <input type="text" class="form-control money" name="residual_value" value="{{ $data->depreciation_method_descr }}" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                </div>
                                            <!--/ form -->
                                        </div>