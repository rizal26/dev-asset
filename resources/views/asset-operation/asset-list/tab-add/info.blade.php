                                        <div class="tab-pane {{ $infoLink=='active'?'active':'fade' }}" id="account-vertical-info" role="tabpanel" aria-labelledby="account-pill-info" aria-expanded="{{ $infoAriaExpand }}">
                                            <!-- form -->
                                            <form class="validate-form" action="{{ route('asset-operation.asset-list.add') }}?tab=info" method="POST">
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Purchase Cost</label>
                                                            <input type="text" class="form-control money" name="purchase_cost" value="{{ old('purchase_cost') }}" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Currency</label>
                                                            <input type="text" class="form-control" name="currency" value="{{ old('currency') }}" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Purchase Date</label>
                                                            <input type="text" id="fp-default" name="purchase_date" value="{{ old('purchase_date') }}" class="form-control flatpickr-basic" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">PO Number</label>
                                                            <input type="text" class="form-control" name="po_number" value="{{ old('po_number') }}" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Supplier</label>
                                                            <select name="supplier_id" class="form-control select2" {{ $disabled }} >
                                                                <option></option>
                                                                @foreach ($supplier as $item)
                                                                    <option value="{{ $item->supplier_id }}" 
                                                                        {{ $item->supplier_id==old('supplier_id')?'selected':'' }}>
                                                                        {{ $item->name }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Residual Value</label>
                                                            <input type="text" class="form-control money" name="residual_value" value="{{ old('residual_value') }}" {{ $disabled }}/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Depreciation Method</label>
                                                            @php
                                                                $arrDeprMetd = [
                                                                    1 => 'Declining Balance',
                                                                    2 => 'Double Declining Balance'
                                                                ];
                                                            @endphp
                                                            <select name="depreciation_method" class="form-control select2" {{ $disabled }}>
                                                                @foreach ($arrDeprMetd as $key => $item)
                                                                    <option value="{{ $key }}" {{ $key==old('depreciation_method')?'selected':'' }}>{{ $item }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <button type="button" id="nextFromInfo" class="btn btn-primary mt-1 mr-1">Next</button>
                                                        <a href="{{ route('asset-operation.asset-list.index') }}" class="btn btn-outline-secondary mt-2">Cancel</a>
                                                    </div>
                                                </div>
                                            </form>
                                            <!--/ form -->
                                        </div>

                                        <script>
                                            $(function () {
                                                $('#nextFromInfo').click(function (e) { 
                                                    e.preventDefault();
                                                    $('#account-vertical-other').addClass('active');                                                    
                                                    $('#account-vertical-other').addClass('fade');                                                    
                                                    $('#account-vertical-other').addClass('show');                                                    
                                                    $('#account-vertical-other').attr('aria-expanded', 'true');
                                                    $('#account-pill-other').addClass('active');                                                    
                                                    $('#account-pill-other').attr('aria-expanded', 'true');
    
                                                    $('#account-vertical-info').removeClass('active');                                                    
                                                    $('#account-vertical-info').removeClass('show');                                                    
                                                    $('#account-vertical-info').removeClass('fade');                                                    
                                                    $('#account-vertical-info').attr('aria-expanded', 'false');                                                    
                                                    $('#account-pill-info').removeClass('active');                                                    
                                                    $('#account-pill-info').attr('aria-expanded', 'false'); 
                                                });
                                            });
                                        </script>