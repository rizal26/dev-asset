@extends('templates.main')
@section('title', $title)
@section('content')
            
                @include('templates.message-validation')
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{$title}}</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form form-horizontal" method="post" action="{{ route('data-master.location.edit', $data->building_id) }}">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="code" class="float-right">District</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <select name="location_id" class="form-control">
                                                            <option></option>
                                                            @foreach ($location as $item)
                                                                <option value="{{ $item->location_id }}" 
                                                                    {{ $item->location_id==$data->location_id?'selected':'' }}>{{ $item->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="name" class="float-right">Location Name</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="text" id="name" class="form-control" name="name" placeholder="Name" value="{{ $data->name }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="latitude" class="float-right">Latitude</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="number" step="any" id="latitude" class="form-control" name="latitude" placeholder="Latitude" value="{{ old('latitude') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="longitude" class="float-right">Longitude</label>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="number" step="any" id="longitude" class="form-control" name="longitude" placeholder="Longitude" value="{{ old('longitude') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-9 offset-sm-3">
                                                <button type="submit" class="btn btn-primary mr-1 waves-effect waves-float waves-light">update</button>
                                                <a href="{{ route('data-master.location.index') }}" class="btn btn-outline-warning waves-effect">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script src="{{asset('app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
                    <script>
                        $(function () {
                            $('select').select2({
                                placeholder: 'Select',
                                allowClear: true
                            });
                        });
                    </script>
@endsection