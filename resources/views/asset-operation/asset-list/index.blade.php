@extends('templates.main')
@section('title', $title)
@section('content')
                @if($errors->any())
                    @foreach($errors->all() as $error)
                        @if($errors->has('success'))
                        <div class="alert alert-success">
                            <div class="alert-body">
                                <strong>{{ $error }}</strong>
                            </div>
                        </div>
                        @else
                        <div class="alert alert-danger">
                            <div class="alert-body">
                                <strong>{{ $error }}</strong>
                            </div>
                        </div>
                        @endif    
                    @endforeach
                @endif
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{ $title }}</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="btn-group">
                                            <a href="{{ route('asset-operation.asset-list.add') }}" class="btn btn-primary waves-effect waves-float waves-light">
                                                Add Asset
                                            </a>
                                            <button class="btn btn-success dropdown-toggle waves-effect waves-float waves-light" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Export
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2" style="">
                                                <a class="dropdown-item" id="exportlabel" href="javascript:void(0);">Label</a>
                                                <a class="dropdown-item" id="exportexcel" href="javascript:void(0);">Excel</a>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-md-1">
                                            <form id="formrow">
                                                <select name="rows" id="rows" class="form-control" style="width: fit-content">
                                                    <option value="10" {{ request()->has('rows')&&request()->get('rows')==10?'selected':'' }}>10</option>
                                                    <option value="50" {{ request()->has('rows')&&request()->get('rows')==50?'selected':'' }}>50</option>
                                                    <option value="100" {{ request()->has('rows')&&request()->get('rows')==100?'selected':'' }}>100</option>
                                                </select>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="pull-right mb-2">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="search" placeholder="Search" value="{{ request()->get('search','') }}" aria-describedby="button-addon2">
                                                    <div class="input-group-append" id="button-addon2">
                                                        <button type="submit" class="btn btn-outline-primary waves-effect" type="button"><i data-feather='search'></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="card-datatable">
                                        <div class="mb-2">
                                            <form id="formlabel" action="{{ route('asset-operation.asset-list.print-label') }}" method="post" target="_blank">
                                                {{ csrf_field() }}
                                                <input type="hidden" id="labelorexcel" name="export">
                                            <table id="datatable" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="selectall">
                                                                <label class="custom-control-label" for="selectall"></label>
                                                            </div>
                                                        </th>
                                                        <th>Image</th>
                                                        <th>Asset Name</th>
                                                        <th>Asset Code</th>
                                                        <th>Brand</th>
                                                        <th>Spesification</th>
                                                        <th>User Dept.</th>
                                                        <th>Location</th>
                                                        <th>Usage Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($data as $item)
                                                        <tr>
                                                            <td class="custom-control-label" for="customCheck{{ $item->asset_id }}">
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input select-id" name="asset_code[]" value="{{ $item->asset_code }}" id="customCheck{{ $item->asset_id }}">
                                                                    <label class="custom-control-label" for="customCheck{{ $item->asset_id }}"></label>
                                                                </div>
                                                            </td>
                                                            <td><button type="button" class="btn btn-icon btn-flat-primary waves-effect" data-id="{{ $item->asset_id }}" data-toggle="modal" data-target="#assetImage" title="Open Image"><i data-feather='image'></i></button></td>
                                                            <td>{{ $item->asset_name }}</td>
                                                            <td><a href="{{ route('asset-operation.asset-list.detail', $item->asset_id) }}">{{ $item->asset_code }}</a></td>
                                                            <td>{{ $item->brand }}</td>
                                                            <td>{{ $item->spec }}</td>
                                                            <td>{{ $item->dept_name }}</td>
                                                            <td>{{ $item->location_name }}</td>
                                                            <td>{{ $item->usage_status }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            </form>
                                        </div>
                                    </div>
                                    <span>Total rows : {{ number_format($data->total(), 0, ',', '.') }}</span>
                                    <div class="float-right">
                                        {{ $data->links('vendor.pagination.bootstrap-4') }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade text-left" id="assetImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel1"></h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="text-center" id="bodyImage">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <img src="" alt="" srcset="">
                    <script src="{{asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
                    <script src="{{asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>

                    <script>
                        $(function () {
                            $('#assetImage').on('show.bs.modal', function (event) {
                                var button = $(event.relatedTarget) 
                                var id = button.data('id')
                                var modal = $(this)
                                var url = '{{ url("asset-operation/asset-list/image") }}'+'/'+id;
                                console.log(url);
                                $.ajax({
                                    type: "get",
                                    url: url,
                                    beforeSend: function() {
                                        var spinner = '<div class="spinner-border" style="width: 3rem; height: 3rem" role="status">'+
                                                            '<span class="sr-only">Loading...</span>'+
                                                        '</div>';
                                        $('#bodyImage').html(spinner)
                                        $('#myModalLabel1').html('');
                                    },
                                    success: function (response) {
                                        var html = '';
                                        var title = '';
                                        if (response.code == 0) {
                                            html = '<img width="400" src="'+response.src+'">';          
                                        } else {
                                            html = response.msg;          
                                        }

                                        $('#myModalLabel1').html(response.title);
                                        $('#bodyImage').html(html);
                                    }
                                });
                            });

                            $('#datatable').DataTable({
                                bFilter: false,
                                bInfo: false,
                                bPaginate: false,
                                scrollX: true,
                                ordering: false
                            });

                            $('#rows').change(function (e) { 
                                e.preventDefault();
                                let val = $(this).val();
                                $('#formrow').submit();
                            });

                            $('#selectall').change(function (e) { 
                                e.preventDefault();
                                if ($(this).is(':checked')) {
                                    $('.select-id').prop('checked',true);
                                } else {
                                    $('.select-id').prop('checked',false);
                                }
                            });

                            $('#exportlabel').click(function (e) { 
                                e.preventDefault();
                                $('#labelorexcel').val('label');
                                $('#formlabel').submit();
                            });

                            $('#exportexcel').click(function (e) { 
                                e.preventDefault();
                                $('#labelorexcel').val('excel');
                                $('#formlabel').submit();
                            });
                        });
                    </script>


@endsection