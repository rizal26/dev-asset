@extends('detail-front.template')
@section('content-detail')
            @php
            $generalLink = '';
            $generalAriaExpand = 'false';

            $locationLink = '';
            $locationAriaExpand = 'false';

            $infoLink = '';
            $infoAriaExpand = 'false';

            $otherLink = '';
            $otherAriaExpand = 'false';

            $maintenanceLink = '';
            $maintenanceAriaExpand = 'false';

            $movementLink = '';
            $movementAriaExpand = 'false';

            if (Request::has('tab')){
                if (Request::get('tab')=='general') {
                    $generalLink = 'active';
                    $generalAriaExpand = 'true';
                } elseif (Request::get('tab')=='location') {
                    $locationLink = 'active';
                    $locationAriaExpand = 'true';
                } elseif (Request::get('tab')=='info') {
                    $infoLink = 'active';
                    $infoAriaExpand = 'true';
                } elseif (Request::get('tab')=='other') {
                    $otherLink = 'active';
                    $otherAriaExpand = 'true';
                } elseif (Request::get('tab')=='maintenance') {
                    $maintenanceLink = 'active';
                    $maintenanceAriaExpand = 'true';
                } elseif (Request::get('tab')=='movement') {
                    $movementLink = 'active';
                    $movementAriaExpand = 'true';
                }
            } else {
                $generalLink = 'active';
                $generalAriaExpand = 'true';
            }
            $disabled = 'disabled';
            @endphp
            <style>
                div.dataTables_wrapper div.dataTables_filter input {
                    margin-left: 0
                }
                #datatableMaintenance_filter, #datatableMaintenance_paginate, 
                #datatableMovement_filter, #datatableMovement_paginate {
                    float: right;
                }
            </style>
            <div class="mb-2">
                <h3><b>DEV - {{ $title }}</b></h3>
            </div>
            <div class="row">
                @include('detail-front.tab.leftmenu')
                <!-- right content section -->
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <div class="tab-content">
                                <!-- general tab -->
                                @include('detail-front.tab.general')
                                <!--/ general tab -->
                                
                                <!-- Location & User -->
                                @include('detail-front.tab.location')
                                <!--/ change password -->
                                
                                <!-- information -->
                                @include('detail-front.tab.info')
                                <!--/ information -->
                                
                                <!-- other information -->
                                @include('detail-front.tab.other')
                                <!-- other information -->

                                <!-- maintenance -->
                                @include('detail-front.tab.maintenance')
                                <!-- maintenance -->

                                <!-- movement -->
                                @include('detail-front.tab.movement')
                                <!-- movement -->
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ right content section -->
            </div>
            @include('detail-front.tab.js')

@endsection