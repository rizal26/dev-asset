<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style>
        .bootstrap-btn {
            display: inline-block;
            font-weight: 400;
            color: #212529;
            text-align: center;
            border: 1px solid transparent;
            padding: .375rem .75rem;
            font-size: 1rem;
            line-height: 1.5;
            border-radius: .25rem;
            color: #fff;
            background-color: #007bff;
            text-decoration: none;
        }


        .bootstrap-btn:hover{
            background-color: #0069d9;

        }
    </style>
    <title>Movement</title>
  </head>
  <body>
    <div style="margin: 30px; font-family: Arial, Helvetica, sans-serif;">
        <p><b>Dear {{ $mailto }},</b></p>
        <p>There is an asset movement request from <b>{{ $created_by }}</b> that require your validation.</p>

        <p>This following is the asset information :</p>
        <table>
            <tbody>
                <tr>
                    <td><b>Asset Code</b></td>
                    <td style="padding-left: 20px;padding-right: 20px;">:</td>
                    <td>{{ $asset_code }}</td>
                </tr>
                <tr>
                    <td><b>Asset Name</b></td>
                    <td style="padding-left: 20px;padding-right: 20px;">:</td>
                    <td>{{ $asset_name }}</td>
                </tr>
            </tbody>
        </table>
        <br>
        <p>For more information please login to your account, or click the button below</p>
        <a href="{{ route('login') }}" class="bootstrap-btn" target="_blank">Login</a>
        <p><b>Best Regards,<br>
        Admin Mitra Berlian Unggas</b><br>
        Jl. Mayang Padmi Kulon No. 31A<br>
        Kota Baru Parahyangan<br>
        Bandung, Jawa Barat<br>
        Indonesia 40553<br>
        +62 22 1000 189
        </p>
    </div>
  </body>
</html>

