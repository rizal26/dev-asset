                                        <div class="tab-pane {{ $otherLink=='active'?'active':'fade' }}" id="account-vertical-other" role="tabpanel" aria-labelledby="account-pill-other" aria-expanded="{{ $otherAriaExpand }}">
                                            <!-- form -->
                                            <form class="validate-form" action="{{ route('asset-operation.asset-list.detail.edit', $data->asset_id) }}?tab=other" method="POST" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                @php
                                                    if($data->life_time && $data->residual_value && $data->purchase_cost) {
                                                        $currentValue = ( $data->purchase_cost - $data->residual_value ) / $data->life_time;
                                                        $currentAge = date('Y') - date('Y', strtotime($data->created_date));
                                                    } else {
                                                        $currentValue = 0;
                                                        $currentAge = '';
                                                    }
                                                @endphp
                                                <div class="row">
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Current Book Value</label>
                                                            <input type="text" class="form-control" value="{{ number_format($currentValue) }}" readonly/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Current Age</label>
                                                            <input type="text" class="form-control" value="{{ $currentAge }}" readonly/>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Upload Document</label>
                                                            <input type="file" name="document" class="form-control" accept=".pdf, .xls, .xlsx, .doc, .docx" {{ $disabled }}>
                                                        </div>
                                                        @if (!is_null($data->document))
                                                            <a href="{{ asset('documents/'.$data->document) }}">Download Document</a><br>
                                                            @if ($disabled == '')
                                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#delete" style="color:red">Delete Document</a>
                                                            @endif
                                                        @endif
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="account-name">Remarks</label>
                                                            <textarea name="remarks" class="form-control" {{ $disabled }}>{{ $data->remarks }}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        @include('asset-operation.asset-list.tab.button')
                                                    </div>
                                                </div>
                                            </form>
                                            <!--/ form -->
                                        </div>

                                        <div class="modal fade text-left" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-scrollable" role="document">
                                                <div class="modal-content">
                                                    <form method="post" action="{{route('asset-operation.asset-list.detail.delete.document', $data->asset_id)}}?tab=other">
                                                    {{csrf_field()}}
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel1">Delete Confirmation</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Are you sure want to delete ?</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-danger">Yes</button>
                                                            <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>