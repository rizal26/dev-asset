<?php

namespace App\Http\Controllers\AssetPurchase;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Mail\PurchaseAsset;
use App\Exports\AssetListExport;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class PurchaseController extends Controller
{
    public function __construct() {
        $this->middleware(function ($request, $next) {
            if (!session('login')) {
                return redirect()->route('login')->withErrors(['error' => 'Please sign in to continue']);
                die();
            };
            return $next($request);
        });
    }

    public function index(Request $req) {
        try {
            $where = ['deleted' => 0];
            if (in_array(session('users')->role, [2, 3])) {
                $where['location_id'] = session('users')->location_id;
                $where['dept_id'] = session('users')->department_id;
            }
            $data = DB::table('v_asset_purchase')
                ->when($req->has("search"), function($q)use($req){
                    return $q->where("asset_name","like","%".$req->get("search")."%")
                        ->orWhere("request_code","like","%".$req->get("search")."%")
                        ->orWhere("brand","like","%".$req->get("search")."%")
                        ->orWhere("category_name","like","%".$req->get("search")."%")
                        ->orWhere("dept_name","like","%".$req->get("search")."%")
                        ->orWhere("priority_name","like","%".$req->get("search")."%")
                        ->orWhere("order_status_name","like","%".$req->get("search")."%")
                        ->orWhere("expected_date_formated","like","%".$req->get("search")."%");
                })
                ->where('deleted', 0)
                ->orderBy('request_id', 'DESC')
                ->paginate($req->has("rows")?$req->get("rows"):10);
            $data->appends(['search' => $req->get("search")]);
            $data->appends(['rows' => $req->get("rows")]);

            $param = [
                'title' => 'Asset Purchase',
                'data' => $data
            ];
            return view('asset-purchase.index', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    public function detail(Request $req) {
        try {
            $progress = [
                'step1' => '',
                'step2' => '',
                'step3' => '',
                'step4' => '',
                'step5' => '',
                'step6' => '',
                'step7' => '',
                'step8' => '',
                'step9' => '',
                'step10' => ''
            ];

            $data = DB::table('v_asset_purchase')->where('request_id', $req->id)->first();
            if ($data->order_status == 1) {
                $progress['step1'] = 'complete';
                $progress['step2'] = 'in-progress';
            } elseif ($data->order_status == 2) {
                $progress['step1'] = 'complete';
                $progress['step2'] = $data->status==0?'complete':'rejected';
                $progress['step3'] = $data->status==0?'in-progress':'';
            } elseif ($data->order_status == 3) {
                $progress['step1'] = 'complete';
                $progress['step2'] = 'complete';
                $progress['step3'] = $data->status==0?'complete':'rejected';
                $progress['step4'] = $data->status==0?'in-progress':'';
            } elseif ($data->order_status == 4) {
                $progress['step1'] = 'complete';
                $progress['step2'] = 'complete';
                $progress['step3'] = 'complete';
                $progress['step4'] = $data->status==0?'complete':'rejected';
                $progress['step5'] = $data->status==0?'in-progress':'';
            } elseif ($data->order_status == 5) {
                $progress['step1'] = 'complete';
                $progress['step2'] = 'complete';
                $progress['step3'] = 'complete';
                $progress['step4'] = 'complete';
                $progress['step5'] = $data->status==0?'complete':'rejected';
                $progress['step6'] = $data->status==0?'in-progress':'';
            } elseif ($data->order_status == 6) {
                $progress['step1'] = 'complete';
                $progress['step2'] = 'complete';
                $progress['step3'] = 'complete';
                $progress['step4'] = 'complete';
                $progress['step5'] = 'complete';
                $progress['step6'] = $data->status==0?'complete':'rejected';
                $progress['step7'] = $data->status==0?'in-progress':'';
            } elseif ($data->order_status == 7) {
                $progress['step1'] = 'complete';
                $progress['step2'] = 'complete';
                $progress['step3'] = 'complete';
                $progress['step4'] = 'complete';
                $progress['step5'] = 'complete';
                $progress['step6'] = 'complete';
                $progress['step7'] = $data->status==0?'complete':'rejected';
                $progress['step8'] = $data->status==0?'in-progress':'';
            } elseif ($data->order_status == 8) {
                $progress['step1'] = 'complete';
                $progress['step2'] = 'complete';
                $progress['step3'] = 'complete';
                $progress['step4'] = 'complete';
                $progress['step5'] = 'complete';
                $progress['step6'] = 'complete';
                $progress['step7'] = 'complete';
                $progress['step8'] = $data->status==0?'complete':'rejected';
                $progress['step9'] = $data->status==0?'in-progress':'';
            } elseif ($data->order_status == 9) {
                $progress['step1'] = 'complete';
                $progress['step2'] = 'complete';
                $progress['step3'] = 'complete';
                $progress['step4'] = 'complete';
                $progress['step5'] = 'complete';
                $progress['step6'] = 'complete';
                $progress['step7'] = 'complete';
                $progress['step8'] = 'complete';
                $progress['step9'] = $data->status==0?'complete':'rejected';
                $progress['step10'] = $data->status==0?'in-progress':'';
            } elseif ($data->order_status == 10) {
                $progress['step1'] = 'complete';
                $progress['step2'] = 'complete';
                $progress['step3'] = 'complete';
                $progress['step4'] = 'complete';
                $progress['step5'] = 'complete';
                $progress['step6'] = 'complete';
                $progress['step7'] = 'complete';
                $progress['step8'] = 'complete';
                $progress['step9'] = 'complete';
                $progress['step10'] = $data->status==0?'complete':'rejected';
            }
            $param = [
                'title' => 'Asset Purchase Request Approval',
                'data' => $data,
                'progress' => $progress,
                'supplier' => DB::table('supplier')->where('deleted', 0)->get(),
                'entity' => DB::table('entity')->where('deleted', 0)->get()
            ];

            return view('asset-purchase.detail', $param);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }

    function sendMail($dataAsset, $apprBy, $status) {
        $insert['req_num'] = $dataAsset->request_id;
        $insert['brand'] = $dataAsset->brand;
        $insert['spec'] = $dataAsset->spec;
        $insert['qty'] = $dataAsset->qty;
        $insert['remarks'] = $dataAsset->remarks;
        $insert['creator_name'] = $dataAsset->creator_name;
        $insert['item_name'] = $dataAsset->asset_name;
        $insert['category_name'] = $dataAsset->category_name;
        $insert['uom_name'] = $dataAsset->uom_name;
        $insert['expected_date'] = $dataAsset->expected_date;
        
        if ($status == 1) {
            $insert['content'] = '<p>Your request has been rejected.</p>';
        } else {
            $insert['content'] = '<p>There is a new asset request that require your approval.</p>';
        }

        foreach ($apprBy as $key => $value) {
            $insert['mailto'] = $value->name;
            Mail::to($value->email)->send(new PurchaseAsset($insert));
        }
    }

    public function step1(Request $req) {
        try {
            $param = [
                'title' => 'Asset Purchase Request Form',
                'm_asset' => DB::table('m_asset')->where('deleted', 0)->get(),
                'uom' => DB::table('uom')->where('deleted', 0)->get(),
                'progress' => [
                    'step1' => 'in-progress',
                    'step2' => '',
                    'step3' => '',
                    'step4' => '',
                    'step5' => '',
                    'step6' => '',
                    'step7' => '',
                    'step8' => '',
                    'step9' => '',
                    'step10' => ''
                ]
            ];

            if ($req->isMethod('POST')) {
                $input = $req->input();
                unset($input['_token']);
                $messages = [
                    'expected_date.required' => 'Expected date field is required',
                ];
                $rules = [
                    'expected_date' => 'required'
                ];
                $validator = Validator::make($input, $rules, $messages);
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput();
                }
                $input['dept_id'] = session('users')->department_id;
                $apprBy = DB::table('users')
                    ->where([
                        'department_id' => session('users')->department_id,
                        'location_id' => session('users')->location_id,
                        'role' => 3, // officer head
                        'deleted' => 0 // officer head
                    ])->get();
                $input['order_status'] = 1;
                $input['created_by'] = session('users')->user_id;
                DB::beginTransaction();
                $save = DB::table('asset_purchase')->insertGetId($input);
                $dataAsset = DB::table('v_asset_purchase')->where('request_id', $save)->first();

                $this->sendMail($dataAsset, $apprBy, 0);
                        
                DB::commit();
                return redirect('asset-purchase/detail/'.$save)->withErrors([
                    'success' => 'Data has been submitted successfully, your Dept. Head will review your approval'
                ])->withInput();
            }

            return view('asset-purchase.step1', $param);
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors([
                'error' => $e->getMessage()
            ])->withInput();
        }
    }

    public function approval(Request $req) {
        try {
            $req_id = $req->id;
            $data = DB::table('v_asset_purchase')->where('request_id', $req_id)->first();
            $from = session('users')->role;
            $comment = $req->comment;
            $input = ['status' => $req->status]; // approve or reject
            $mailto = [];
            if ($from == 3) {
                $input['appr_dept_head'] = $from;
                $input['cmt_dept_head'] = $comment;
                $input['order_status'] = 2; //approved from dept. head
                $mailto['role'] = 7;
            } elseif ($from == 7 && $data->order_status != 9) {
                $input['appr_asset_mgt'] = $from;
                $input['cmt_asset_mgt'] = $comment;
                $input['order_status'] = 3; //approved from asset mgt.
                $mailto['role'] = 4;
            } elseif ($from == 4 && $data->order_status != 9 && $data->order_status != 8) {
                $maxBudget = intval( str_replace(',','', $req->max_budget) );
                $input['appr_finance'] = $from;
                $input['cmt_finance'] = $comment;
                if ($maxBudget >= 100000000) {
                    $input['order_status'] = 4; //to finance director
                    $mailto['role'] = 5;
                } else {
                    $input['order_status'] = 5; //to purchasing
                    $mailto['role'] = 6;
                }
                $input['max_budget'] = $maxBudget;
                $input['payment_method'] = $req->payment_method;
            } elseif ($from == 5) {
                $input['appr_finance_director'] = $from;
                $input['cmt_finance_director'] = $comment;
                $input['order_status'] = 5; //approved from finance director
                $mailto['role'] = 6;
            } elseif ($from == 6) {
                $amount = intval( str_replace(',','', $req->amount) );
                $input['amount'] = $amount;
                $input['advance_payment'] = $req->advance_payment;
                $input['supplier_id'] = $req->supplier_id;
                $input['appr_purchasing'] = $from;
                $input['cmt_purchasing'] = $comment;
                $input['order_status'] = 6; //approved from purchasing officer
                $mailto['user_id'] = $data->created_by;
            } elseif ($from == 2 && $data->order_status == 6) {
                $input['appr_user_asset'] = $from;
                $input['cmt_user_asset'] = $comment;
                $input['order_status'] = 8; //approved by user
                $mailto['role'] = 4;
            } elseif ($from == 4 && $data->order_status == 8) {
                $actualCost = intval( str_replace(',','', $req->actual_cost) );
                $resVal = intval( str_replace(',','', $req->residual_value) );
                $input['order_status'] = 9; //approved from finance
                $input["depreciation_method"] = $req->depreciation_method;
                $input["life_time"] = $req->life_time;
                $input["actual_cost"] = $actualCost;
                $input["residual_value"] = $resVal;
                $input["entity_id"] = $req->entity_id;
                $input["type"] = $req->type;

                if($req->invoice != '' || !is_null($req->invoice)){
                    $rules = [
                        'invoice' => 'mimes:pdf|max:5120',
                    ];
                    $validator = Validator::make($req->file(), $rules);
                    if ($validator->fails()) {
                        return redirect()->back()->withErrors($validator->errors())->withInput();
                    }
                    
                    $path = public_path().'/documents';
                    $file = $req->invoice;
                    $ext  = $file->getClientOriginalExtension();
                    $filename = 'INV'.$req->id.'-'.date('dmyHis').'.'.$ext;
                    $file->move($path, $filename);
                    $input['invoice'] = $filename;
                } else {
                    unset($input['invoice']);
                }
                $mailto['role'] = 7;
            } elseif ($from == 7 && $data->order_status == 9) {
                $input['order_status'] = 10; //approved from asset mgt.
                $update = DB::table('asset_purchase')
                    ->where('request_id', $req_id)
                    ->update($input);
                return redirect('/asset-purchase')->withErrors([
                    'success' => 'Request has been recognized successfully, please validate asset and make sure has been according to the data'
                ])->withInput();
            }
            $mailto['deleted'] = 0;

            if ($req->status == 1) {
                $apprBy = DB::table('users')
                ->where('user_id', $data->created_by)->get();
            } else {
                $apprBy = DB::table('users')
                    ->where($mailto)->get();
            }
                
            $update = DB::table('asset_purchase')
                ->where('request_id', $req_id)
                ->update($input);

            $this->sendMail($data, $apprBy, $req->status);

            return redirect()->back()->withErrors([
                'success' => 'Data has been approved successfully'
            ])->withInput();

        } catch (\Exception $e) {
            return redirect()->back()->withErrors([
                'error' => $e->getMessage()
            ])->withInput();
        }
    }

    public function reject(Request $req) {
        try {
            //code...
        } catch (\Exception $e) {
            return redirect()->back()->withErrors([
                'error' => $e->getMessage()
            ])->withInput();
        }
    }

    public function validation(Request $req) {
        try {
            $req_id = $req->id;
            $update = DB::table('asset_purchase')
                ->where('request_id', $req_id)
                ->update([
                    'validate' => 1
                ]);
            $data = DB::table('v_asset_purchase')
                ->where('request_id', $req_id)
                ->first();
            
            $user = DB::table('users')
                ->where(['user_id' => $data->created_by, 'deleted' => 0])
                ->first();
            
            $entityCategory = $data->entity_code.'.'.$data->category_code;
            
            for ($i=0; $i < intval($data->qty); $i++) { 
                $lastAsset = DB::table('asset_list')
                    ->where('item_code', 'like', '%'.$entityCategory.'%')
                    ->where('deleted', 0)
                    ->select('item_code')
                    ->get();
                $numberCode = [0];
                if (count($lastAsset) > 0) {
                    foreach ($lastAsset as $key => $value) {
                        $curentNumber = explode('.', $value->item_code)[3];
                        $numberCode[] = intval(ltrim($curentNumber, '0'));
                    }
                }
                rsort($numberCode);
                $numberGiven = $numberCode[0]+1;
                $increment = str_pad($numberGiven, 5, '0', STR_PAD_LEFT);
                $itemCode = $entityCategory.'.'.date('my', strtotime($data->updated_at)).'.'.$increment;
                $toAssetList = [
                    "item_code" => $itemCode,
                    "m_asset_id" => $data->m_asset_id,
                    "brand" => $data->brand,
                    "spec" => $data->spec,
                    "type" => $data->type,
                    "qty" => 1,
                    "condition" => 1,
                    "uom_id" => $data->uom,
                    "location_id" => $user->location_id,
                    "usage_status" => 1,
                    "pr_number" => 'PR-'.str_pad($data->request_id, 5, '0', STR_PAD_LEFT),
                    "po_number" => 'PO-'.str_pad($data->request_id, 5, '0', STR_PAD_LEFT),
                    "document" => $data->invoice,
                    "department_id" => $user->department_id,
                    "entity_id" => $data->entity_id,
                    "purchase_cost" => $data->actual_cost,
                    "currency" => "IDR",
                    "purchase_date" => date('Y-m-d H:i:s', strtotime($data->updated_at)),
                    "supplier_id" => $data->supplier_id,
                    "residual_value" => $data->residual_value,
                    "life_time" => $data->life_time,
                    "depreciation_method" => $data->depreciation_method,
                    "created_by" => $data->created_by
                ];

                $save = DB::table('asset_list')->insert($toAssetList);
            }

            return redirect()->back()->withErrors([
                'success' => 'Data has been validated successfully, now you can check on asset list'
            ])->withInput();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors([
                'error' => $e->getMessage()
            ])->withInput();
        }
    }
}
